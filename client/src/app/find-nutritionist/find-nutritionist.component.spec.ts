import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindNutritionistComponent } from './find-nutritionist.component';

describe('FindNutritionistComponent', () => {
  let component: FindNutritionistComponent;
  let fixture: ComponentFixture<FindNutritionistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindNutritionistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindNutritionistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
