import { Component, Input, OnInit } from '@angular/core';
import {Recipe} from '../models/recipe.model'
import { Observable, Subscription } from 'rxjs';
import { RecipeInfo } from '../models/recipeInfo.model';
import { RecipeService } from '../services/recipe.service';
import { AuthUserService } from 'src/app/regular-users/services/auth-reg-user.service';
import { User } from 'src/app/regular-users/models/user.model';
import { Router } from '@angular/router';
import { TrainerAuthService } from 'src/app/health-fitness-specialists/trainers/services/trainer-auth.service';
import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';
import { NutritionistAuthService } from 'src/app/health-fitness-specialists/nutritionists/services/nutritionist-auth.service';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit {
  
  show : boolean = false;

  @Input()
  recipe: Recipe;

  recipeInfo: Observable<RecipeInfo>;
  
  private user: User = null;
  private userSub: Subscription;
  private nutritionist: HealthFitnessSpecialist = null;
  private nutritionistSub: Subscription;
  private trainer: HealthFitnessSpecialist = null;
  private trainerSub: Subscription;

  constructor(private router : Router,private authUserService : AuthUserService, private authTrainerService : TrainerAuthService, private authNutritionistService : NutritionistAuthService, private recipeService : RecipeService) {
    this.userSub = this.authUserService.user.subscribe((user: User) => {
      this.user = user;
    });
    this.authUserService.sendUserDataIfExists();
    
    //trainer
    this.trainerSub = this.authTrainerService.specialist.subscribe((specialist: HealthFitnessSpecialist) => {
      this.trainer = specialist;
    });
    this.authTrainerService.sendSpecialistDataIfExists();

    //nutritionist
    this.nutritionistSub = this.authNutritionistService.specialist.subscribe((specialist: HealthFitnessSpecialist) => {
      this.nutritionist = specialist;
    });
    this.authNutritionistService.sendSpecialistDataIfExists();
    
   }

  ngOnInit() {
  }

  public readMore() : void 
  {
    if(this.user || this.trainer || this.nutritionist)
    {
      
        this.router.navigate(['/recipe-info'], {
          queryParams: {
            recipeId:this.recipe.id
          }
        });
    }
    else
    {
      window.alert("Please log in to see more about this recipe!");
    }
  }

}
