import { Component, OnInit } from '@angular/core';
import { Recipe } from './models/recipe.model';
import { Observable } from 'rxjs';
import { RecipeService } from './services/recipe.service';
import { Joke } from './models/recipeInfo.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-find-recipe',
  templateUrl: './find-recipe.component.html',
  styleUrls: ['./find-recipe.component.css']
})
export class FindRecipeComponent implements OnInit {

  ingredients : string;
  recipes: Observable<Recipe[]>;
  joke : Observable<Joke>;
  jokeText : string;
  

  inputFieldClass: string;
  constructor(private recipeService : RecipeService, private router : Router) {

      this.recipes = this.recipeService.getRecipes(this.ingredients);
      this.joke = this.recipeService.getJoke();
      const sub = this.joke.subscribe((j : Joke)=> {
        this.jokeText = j.text;
      })
     
  }

  ngOnInit() {

  }

  public addIngredient() : void {

    var newIngredient : string = (document.getElementById("ing_value") as HTMLInputElement).value;
    if (this.ingredients){
      this.ingredients = this.ingredients.concat(", ");
      this.ingredients = this.ingredients.concat(newIngredient);
    }
    else
    this.ingredients = newIngredient;
    console.log(this.ingredients);
    this.recipes= this.recipeService.getRecipes(this.ingredients);
  }

  public ingredientsToArray() : string[] {
    if(this.ingredients === "")
    return [""]
    return this.ingredients.split(',');
  }


  public removeIngredient(ing : string) {
   this.ingredients = this.ingredients.replace(ing, '');
   if(this.ingredients.endsWith(','))
   this.ingredients = this.ingredients.substr(0, this.ingredients.length-1);
   
   if(this.ingredients.charAt(0) == ',')
   this.ingredients = this.ingredients.substr(1, this.ingredients.length);

   this.ingredients = this.ingredients.replace(",,", ",");

  }

  public searchForRecipes() : void
  {
    if(this.ingredients)
    {
    this.router.navigate(['/recipe-list'], {
      queryParams: {
        ingredients:this.ingredients
      }
    });
  }
    else 
    {
      window.alert("You have to add at least one ingredient!");
    }
  }

}
