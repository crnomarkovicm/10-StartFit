import { Recipe } from './recipe.model';

export interface ApiRecipe {
    id: number;
    title: string;
    image: string;
    imageType: string;
    usedIngredientCount: number;
    missedIngredientCount: number;
    missedIngredients: any;
}

export interface RecipePagination {
    docs: ApiRecipe[];
}


export interface ApiRecipeInfo {
    vegeterian: boolean;
    vegan : boolean;
    glFree: boolean;
    dairyFree : boolean;
    veryHealthy : any;
    cheap : any;
    veryPop: any;
    sust : any;
    wwsp : any;
    gaps : any;
    lfm: any;
    prepTime: number;
    cookingTime: number;
    aggLikes: any;
    spScore: any;
    healthScore: number;
    credits: any;
    source: any;
    price: any;
    extendedIngredients: any;
    id: number;    
    title : string;
    readyInMinutes: number;
    servings: any;
    sourceUrl: any;
    image: string;
    imgType: any;
    summary : any;
    cuisines: any;
    dishTypes: string[];
    diets: any;
    occ: any;
    wine: any;
    instructions: string;
    an: any;
    originalId: any;

}