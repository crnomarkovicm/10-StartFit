export interface IRecipeInfo{
  idRecipe: string,
  name : string,
  summary: string,
  ingredients: string[],
  instructions: string,
  prepTime: number,
  score: number,
  dishTypes: string[],
  image: string,
  vegeterian: boolean,
  vegan : boolean,
  glFree: boolean,
  dairyFree : boolean
}

export enum HealthScore {
  Excellent = 'Excellent',
  Good = 'Good',
  Average = 'Average',
  Bad = 'Bad',
  NotRated = 'Not rated'
}


export interface ApiJoke {
  text : string;
}

export class Joke {
  constructor(
    public text : string
  ) {}
}


export class RecipeInfo {

    constructor(
        public idRecipe: string,
        public name : string,
        public summary: string,
        public ingredients: string[],
        public instructions: string,
        public prepTime: number,
        public score: number,
        public dishTypes: string[],
        public image: string,
        public vegeterian: boolean,
        public vegan : boolean,
        public glFree: boolean,
        public dairyFree : boolean,
      ){

    }

  }
