import { Component, Input, OnInit } from '@angular/core';
import {Recipe} from '../models/recipe.model'
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RecipeService } from '../services/recipe.service';
import { RecipeInfo } from '../models/recipeInfo.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  
  recipes : Observable<Recipe[]>;


  ingredients : string = "";

  constructor(private recipeService : RecipeService, private activateRouter :ActivatedRoute) { 
    
}
  ngOnInit() {
    this.activateRouter.queryParams.subscribe(
      params => {
        console.log(params.ingredients);
        this.ingredients = params.ingredients;
      }
    );
      this.recipes = this.recipeService.getRecipes(this.ingredients);
 
    



  }

}
