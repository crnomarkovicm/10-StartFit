import { Component, Input, OnInit } from '@angular/core';
import { RecipeInfo, HealthScore } from '../models/recipeInfo.model';
import { User } from 'src/app/regular-users/models/user.model';
import { Subscription } from 'rxjs';
import { AuthUserService } from 'src/app/regular-users/services/auth-reg-user.service';
import { RecipeService } from '../services/recipe.service';
import { ActivatedRoute } from '@angular/router';
import { TrainerAuthService } from 'src/app/health-fitness-specialists/trainers/services/trainer-auth.service';
import { NutritionistAuthService } from 'src/app/health-fitness-specialists/nutritionists/services/nutritionist-auth.service';
import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';

@Component({
  selector: 'app-recipe-info',
  templateUrl: './recipe-info.component.html',
  styleUrls: ['./recipe-info.component.css']
})
export class RecipeInfoComponent implements OnInit {

  RatingEnum = HealthScore;
  private url : string;
  private recipeInfo : RecipeInfo;
  private recipeSub: Subscription;
  private recipeId : number;  
  private user: User = null;
  private userSub: Subscription;
  private trainer: HealthFitnessSpecialist = null;
  private trainerSub: Subscription;
  private nutritionist: HealthFitnessSpecialist = null;
  private nutritionistSub: Subscription;
  constructor(private authUserService : AuthUserService, private authTrainerService : TrainerAuthService, private authNutritionistService : NutritionistAuthService, private recipeService : RecipeService, private activateRouter :ActivatedRoute) {
    this.userSub = this.authUserService.user.subscribe((user: User) => {
      this.user = user;
    });
    this.authUserService.sendUserDataIfExists();

    //trainer
    this.trainerSub = this.authTrainerService.specialist.subscribe((specialist: HealthFitnessSpecialist) => {
      this.trainer = specialist;
    });
    this.authTrainerService.sendSpecialistDataIfExists();

    //nutritionist
    this.nutritionistSub = this.authNutritionistService.specialist.subscribe((specialist: HealthFitnessSpecialist) => {
      this.nutritionist = specialist;
    });
    this.authNutritionistService.sendSpecialistDataIfExists();

    
   }

  ngOnInit() {
    this.activateRouter.queryParams.subscribe(
      params => {
        console.log(params.recipeId)
        this.recipeId = params.recipeId;
      }
    );
    this.recipeSub = this.recipeService.getRecipeInfo(this.recipeId).subscribe((recipe : RecipeInfo) => {
      this.recipeInfo = recipe;
    });

  }

  ngOnDestroy() {
    this.userSub ? this.userSub.unsubscribe() : null;
    this.nutritionistSub ? this.nutritionistSub.unsubscribe() : null;
    this.trainerSub ? this.trainerSub.unsubscribe() : null;
  }

  addToFavorites() {
    if(this.user)
      this.url = `http://localhost:3000/api/customers/username/${this.user.username}/recipes`;
    if(this.trainer)
      this.url = `http://localhost:3000/api/trainers/username/${this.trainer.username}/recipes`;
    if(this.nutritionist)
      this.url = `http://localhost:3000/api/nutritionists/username/${this.nutritionist.username}/recipes`;
    
    if(this.recipeInfo)
    {
        this.recipeService.postRecipe(this.recipeInfo, this.url);
        window.alert(`${this.recipeInfo.name} added to your favorite recipes!`);
    }
  }

  stringToHtml(str : string) : any
  {
     
    document.getElementById("summary").innerHTML=str;
    
  }

  public healthScoreRating(): HealthScore {

    if (this.recipeInfo.score === 0 ) {
      return HealthScore.NotRated;
    }
    if (this.recipeInfo.score > 4) {
      return HealthScore.Excellent;
    }
    if (this.recipeInfo.score > 3 ) {
      return HealthScore.Good;
    }
    if (this.recipeInfo.score > 2 ) {
      return HealthScore.Average;
    }
    return HealthScore.Bad;
  }
}


