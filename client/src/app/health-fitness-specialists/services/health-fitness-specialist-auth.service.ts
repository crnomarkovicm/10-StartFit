import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from 'src/app/common/server-error/server-error';
import { JwtService } from 'src/app/common/services/jwt.service';
import { IJWTHealthFitnessSpecialistTokenData } from 'src/app/common/services/models/jwt-token-data';
import { TokenId } from 'src/app/common/services/models/jwt-token-ids';
import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';

@Injectable({
  providedIn: 'root'
})
export class HealthFitnessSpecialistAuth {

  public readonly specialistSubject: Subject<HealthFitnessSpecialist> = new Subject<HealthFitnessSpecialist>();
  public readonly specialist: Observable<HealthFitnessSpecialist> = this.specialistSubject.asObservable();
  public tokenId: TokenId;
  public registerUrl: string;
  public loginUrl: string;
  private isSpecialistLoggedIn: boolean;

  constructor(private http: HttpClient, private jwtService: JwtService) {}

  public sendSpecialistDataIfExists(): HealthFitnessSpecialist {
    const payload: IJWTHealthFitnessSpecialistTokenData = this.jwtService.getHealthFitnessSpecialistDataFromToken(this.tokenId);
    const specialist: HealthFitnessSpecialist = payload
      ? new HealthFitnessSpecialist(payload.id, payload.username, payload.name, payload.surname,
        payload.email, payload.gender, payload.city, payload.municipality,
        payload.dateOfBirth, payload.imgUrl, payload.score, payload.category, payload.approximatePrice, payload.marks, payload.numberOfCustomer)
      : null;
    this.specialistSubject.next(specialist);
    this.isSpecialistLoggedIn = specialist !== null;
    return specialist;
  }

  public get specialistLoggedIn(): boolean {
    return this.isSpecialistLoggedIn;
  }

  public registerSpecialist(username: string, password: string, name: string, surname: string,
    email: string, gender: string, city: string, municipality: string, dateOfBirth: string, imgUrl: string, category: [string], approximatePrice: number): Observable<HealthFitnessSpecialist> {

    const body = { username, name, surname, email, password, gender, city, municipality, dateOfBirth, imgUrl, category, approximatePrice };
    return this.http.post<{ token: string }>(this.registerUrl, body).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService, this.tokenId)),
      map((response: { token: string }) => this.mapResponseToSpecialist(response))
    );
  }

  public signInSpecialist(username: string, password: string): Observable<HealthFitnessSpecialist> {
    const body = { username, password };
    return this.http.post<{ token: string }>(this.loginUrl, body).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService, this.tokenId)),
      map((response: { token: string }) => this.mapResponseToSpecialist(response))
    );
  }

  public logoutSpecialist(): void {
    this.jwtService.removeToken(this.tokenId);
    this.isSpecialistLoggedIn = false;
    this.specialistSubject.next(null);
  }

  public mapResponseToSpecialist(response: { token: string }): HealthFitnessSpecialist {
    this.jwtService.setToken(response.token, this.tokenId);
    return this.sendSpecialistDataIfExists();
  }
}
