import { TestBed } from '@angular/core/testing';

import { HealthFitnessSpecialistAuth } from './health-fitness-specialist-auth.service';

describe('HealthFitnessSpecialistAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HealthFitnessSpecialistAuth = TestBed.get(HealthFitnessSpecialistAuth);
    expect(service).toBeTruthy();
  });
});
