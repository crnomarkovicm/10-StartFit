import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { JwtService } from 'src/app/common/services/jwt.service';
import { TrainerAuthService } from './trainer-auth.service';
import { catchError, map } from 'rxjs/operators';
import { HealthFitnessSpecialist } from '../../models/healthFitnessSpecialist.model';
import { Observable } from 'rxjs';
import { Recipes } from 'src/app/profile/favorite-recipes/favorite-recipes.model';
import { TrainerData, TrainerPagination } from './models/trainer-pagination';
import { TokenId } from 'src/app/common/services/models/jwt-token-ids';
import { handleError } from 'src/app/common/server-error/server-error';

@Injectable({
  providedIn: 'root'
})


export class TrainerService {

  constructor(private http: HttpClient,
    private jwtService: JwtService,
    private auth: TrainerAuthService) { }


  public patchSpecialistPassword(username: string, oldPassword: string, newPassword: string): Observable<HealthFitnessSpecialist> {
    const body = { username, oldPassword, newPassword };
    const patchSpecialistPasswordUrl = `http://localhost:3000/api/trainers/username/${username}`;
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken(this.auth.tokenId)}`);
    return this.http.patch<{ token: string }>(patchSpecialistPasswordUrl, body, { headers }).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService, TokenId.TRAINER_ID)),
      map((response: { token: string }) => this.auth.mapResponseToSpecialist(response)),
    );
  }

  public patchSpecialistProfileImage(file: File): Observable<HealthFitnessSpecialist> {
    const body: FormData = new FormData();
    body.append("file", file);
    const patchSpecialistImgUrl = `http://localhost:3000/api/trainers/profile-image/`;

    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken(this.auth.tokenId)}`);
    return this.http.patch<{ token: string }>(patchSpecialistImgUrl, body, { headers }).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService, TokenId.TRAINER_ID)),
      map((response: { token: string }) => this.auth.mapResponseToSpecialist(response)),
    );
  }

  public getTrainersRecipes(username: string): Observable<Recipes[]>{
    return this.http.get<Recipes[]>(`http://localhost:3000/api/trainers/username/${username}/recipes`);
  }

  public deleteRecipe(username: string, id: string):Observable<HealthFitnessSpecialist> {
    return this.http.put<HealthFitnessSpecialist>(`http://localhost:3000/api/trainers/username/${username}/recipes/${id}`, {});
 }

  public getTrainers(page: number = 1, limit: number = 10, min: number, max:number, city: string, municipality: string, category:[string], sort: string): Observable<HealthFitnessSpecialist[]> {
    const params: HttpParams = new HttpParams()
    .append('page', page.toString())
    .append('limit', limit.toString());
    const body = {min, max, city, municipality, category, sort }
    console.log(body);
    const obs: Observable<TrainerPagination> = this.http.post<TrainerPagination>(
      "http://localhost:3000/api/trainers",
      body,
      {
        params
      }
    );

    const newObs: Observable<HealthFitnessSpecialist[]> = obs.pipe(
      map((pagination: TrainerPagination) => {
        return pagination.docs.map (
          (doc : TrainerData) =>
          new HealthFitnessSpecialist(
              doc._id,
              doc.username,
              doc.name,
              doc.surname,
              doc.email,
              doc.gender,
              doc.location.city,
              doc.location.municipality,
              doc.dateOfBirth,
              doc.imgUrl,
              doc.score,
              doc.category,
              doc.approximatePrice,
              doc.numberOfCustomer,
              doc.marks
            )

        );
      })
    );

    return newObs;
  }

  public postTrainer(trainer_id: string, user_username : string){
    this.http.put<any>(`http://localhost:3000/api/customers/username/${user_username}/addTrainer/${trainer_id}`, "").subscribe(data => console.log(data));
  }

  public rateTrainer(trainer_username: string, mark: number){
    this.http.put<any>(`http://localhost:3000/api/customers/rateTrainer/${trainer_username}`, {mark}).subscribe(data => console.log(data));
  }
}
