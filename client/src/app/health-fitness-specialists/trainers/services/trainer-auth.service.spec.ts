import { TestBed } from '@angular/core/testing';

import { TrainerAuthService } from './trainer-auth.service';

describe('TrainerAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrainerAuthService = TestBed.get(TrainerAuthService);
    expect(service).toBeTruthy();
  });
});
