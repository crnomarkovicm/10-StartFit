import { Component, OnInit, Input } from '@angular/core';
import { HealthFitnessSpecialist, HealthFitnessSpecialistRating } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';
import { User } from 'src/app/regular-users/models/user.model';
import { Subscription } from 'rxjs';
import { AuthUserService } from 'src/app/regular-users/services/auth-reg-user.service';
import { Router } from '@angular/router';
import { TrainerService } from '../services/trainer.service';
import { MatSnackBar } from '@angular/material/snack-bar';

declare const $: any;

@Component({
  selector: 'app-trainer-info',
  templateUrl: './trainer-info.component.html',
  styleUrls: ['./trainer-info.component.css']
})
export class TrainerInfoComponent implements OnInit {

  RatingEnum = HealthFitnessSpecialistRating;
  @Input() trainer: HealthFitnessSpecialist;

  private user: User = null;
  private userSub: Subscription;
  public isSomeoneLoggedIn: boolean;
  ratingIsPossible: boolean = true;
  addingIsPossible: boolean = true;
  rating: number = 0;
  starCount: number = 5;
  ratingArr: boolean[] = [];
  snackBarDuration: number = 2000;
  response = [
    1,
    2,
    3,
    4,
    5
  ];

  constructor(private snackBar: MatSnackBar,private trainerService: TrainerService, private authUserService: AuthUserService, private router: Router) {

    this.ratingArr = Array(this.starCount).fill(false);

    //user
    this.userSub = this.authUserService.user.subscribe((user: User) => {
      this.user = user;
    });
    this.authUserService.sendUserDataIfExists();

    this.setisSomeoneLoggedIn();
  }


  returnStar(i: number) {
    if(this.rating >= i+1){
      return 'star';
    }
    else {
      return 'star_border';
    }
  }

  onClick(i: number) {
    if(this.ratingIsPossible){
      this.rating = i + 1;
      this.snackBar.open('You rated the trainer ' + this.trainer.name + ' ' + this.trainer.surname + ' with a rating of ' + this.response[i].toString(),'', {
        duration: this.snackBarDuration,
        panelClass: ['snack-bar']
      });
      this.ratingIsPossible = false;
      this.trainerService.rateTrainer(this.trainer.username, this.response[i]);
    }
  }

  public trainerRating(): HealthFitnessSpecialistRating {

    if (this.trainer.marks === 0 ) {
      return HealthFitnessSpecialistRating.NotRated;
    }
    if (this.trainer.marks/this.trainer.numberOfCustomer > 4) {
      return HealthFitnessSpecialistRating.Excellent;
    }
    if (this.trainer.marks/this.trainer.numberOfCustomer > 3 ) {
      return HealthFitnessSpecialistRating.Good;
    }
    if (this.trainer.marks/this.trainer.numberOfCustomer > 2 ) {
      return HealthFitnessSpecialistRating.Average;
    }
    return HealthFitnessSpecialistRating.Bad;
  }

  private setisSomeoneLoggedIn() : void {
    if (this.authUserService.userLoggedIn)
      this.isSomeoneLoggedIn = true;
    else
      this.isSomeoneLoggedIn = false;
  }

  ngOnDestroy() {
    this.userSub ? this.userSub.unsubscribe() : null;
  }
  ngOnInit() {
    $('.ui.radio.checkbox').checkbox();
  }

  public trainerAddToFavorits(){
    this.addingIsPossible = false;
    this.trainerService.postTrainer(this.trainer.id, this.user.username);
  }
  getColorForCategory(indeks):string{
    return indeks? "#d96846": "#013328";
  }
}
