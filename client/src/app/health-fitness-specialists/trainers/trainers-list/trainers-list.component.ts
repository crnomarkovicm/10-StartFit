import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { TrainerService } from '../services/trainer.service';
import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-trainers-list',
  templateUrl: './trainers-list.component.html',
  styleUrls: ['./trainers-list.component.css']
})
export class TrainersListComponent implements OnInit {


  @Input() trainer: Observable<HealthFitnessSpecialist[]>;

  trainersObservable: Observable<HealthFitnessSpecialist[]>;
  trainers: HealthFitnessSpecialist[];
  page: number = 1;
  limit: number = 10;
  city: string;
  municipality: string;
  categories: [string];
  min: number;
  max: number;
  sort: string;
  numberOfTrainersOnPage: number;

  constructor(private trainerService: TrainerService, private activateRouter: ActivatedRoute) {
    this.activateRouter.queryParams.subscribe(
      params => {
        this.city = params.city;
        this.municipality = params.municipality;
        this.min = params.min;
        this.max = params.max;
        this.categories = params.categories;
        this.sort = params.sort;
      }

    );

    this.trainersObservable = this.trainerService.getTrainers(this.page, this.limit, this.min, this.max, this.city, this.municipality, this.categories, this.sort);
    console.log("Trainers:",this.trainers);

    const sub = this.trainersObservable.subscribe((t : HealthFitnessSpecialist[])=> {
      this.trainers = t;
    });
    //this.numberOfTrainersOnPage = this.trainers.length;
  }

  ngOnInit() {
  }

  public next(): void {
    if(this.trainers.length == 10){
      this.page += 1;
      this.trainersObservable = this.trainerService.getTrainers(this.page, this.limit, this.min, this.max, this.city, this.municipality, this.categories, this.sort);
      const sub = this.trainersObservable.subscribe((t : HealthFitnessSpecialist[])=> {
        this.trainers = t;
      });
    }
  }

  public previous(): void {
    if(this.page > 1){
      this.page -= 1;
      this.trainersObservable = this.trainerService.getTrainers(this.page, this.limit, this.min, this.max, this.city, this.municipality, this.categories, this.sort);
      const sub = this.trainersObservable.subscribe((t : HealthFitnessSpecialist[])=> {
        this.trainers = t;
      });
    }
  }
}
