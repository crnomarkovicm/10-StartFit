import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninTrainerComponent } from './signin-trainer.component';

describe('SigninTrainerComponent', () => {
  let component: SigninTrainerComponent;
  let fixture: ComponentFixture<SigninTrainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninTrainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninTrainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
