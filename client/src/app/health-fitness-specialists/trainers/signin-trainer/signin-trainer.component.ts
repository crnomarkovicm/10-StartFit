import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TrainerAuthService } from '../services/trainer-auth.service';

@Component({
  selector: 'app-signin-trainer',
  templateUrl: './signin-trainer.component.html',
  styleUrls: ['./signin-trainer.component.css']
})
export class SigninTrainerComponent implements OnInit {

  trainerSigninForm: FormGroup;
  signInSub: Subscription;

  constructor(private authService: TrainerAuthService,
    private router: Router) {
    this.trainerSigninForm = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {
  }

  public signIn(): void {

    if (this.trainerSigninForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const data = this.trainerSigninForm.value;
    this.signInSub = this.authService.signInSpecialist(data.username, data.password).subscribe(() => {
      this.router.navigateByUrl("/");
    });
    
  }
  focusUsername(): string{
    if(this.trainerSigninForm.value.username)
      return 'focus';
    return 'blur';
  }
  focusPassword(): string{
    if(this.trainerSigninForm.value.password)
      return 'focus';
    return 'blur';
  }
  
}
