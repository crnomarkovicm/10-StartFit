export enum TrainerCategory {
  Pilates = 'Pilates',
  Aerobic = 'Aerobic',
  Yoga = 'Yoga',
  Cardio = 'Cardio',
  CrossFit = 'CrossFit',
  GroupTraining = 'Group Training',
  PersonalTraining = 'Personal Training',
  Zumba = 'Zumba',
  Box = 'Box',
  BodyBuilding = 'Body Building',
  Group = 'Group',
  Personal = 'Personal'
}

export enum NutritionistCategory {
  LowCarb = 'Low-Carb',
  Chrono = 'Chrono',
  Vegan = 'Vegan',
  Vegeterain = 'Vegeterian',
  UltraLowFat = 'Ultra-Low-Fat',
  Keto = 'Keto',
  Piscaterian = 'Piscaterian'
}


