import { User } from "src/app/regular-users/models/user.model";

export enum HealthFitnessSpecialistRating {
  Excellent = 'Excellent',
  Good = 'Good',
  Average = 'Average',
  Bad = 'Bad',
  NotRated = 'Not rated'
}

export class HealthFitnessSpecialist extends User {

  constructor(public id: string,
              public username: string,
              public name: string,
              public surname: string,
              public email: string,
              public gender: string,
              public city: string,
              public municipality: string,
              public dateOfBirth: string,
              public _imgUrl: string,
              public score: number,
              public category: [string],
              public approximatePrice: number,
              public numberOfCustomer: number,
              public marks: number)
  {
      super(id, username, name, surname, email, gender, city,municipality, dateOfBirth, _imgUrl);

  }
}
