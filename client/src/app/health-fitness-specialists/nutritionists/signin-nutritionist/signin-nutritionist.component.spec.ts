import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninNutritionistComponent } from './signin-nutritionist.component';

describe('SigninNutritionistComponent', () => {
  let component: SigninNutritionistComponent;
  let fixture: ComponentFixture<SigninNutritionistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninNutritionistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninNutritionistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
