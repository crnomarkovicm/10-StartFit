import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NutritionistAuthService } from '../services/nutritionist-auth.service';

@Component({
  selector: 'app-signin-nutritionist',
  templateUrl: './signin-nutritionist.component.html',
  styleUrls: ['./signin-nutritionist.component.css']
})
export class SigninNutritionistComponent implements OnInit {

  nutritionistSigninForm: FormGroup;
  signInSub: Subscription;

  constructor(private authService: NutritionistAuthService,
    private router: Router) {
    this.nutritionistSigninForm = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {
  }

  public signIn(): void {

    if (this.nutritionistSigninForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const data = this.nutritionistSigninForm.value;
    this.signInSub = this.authService.signInSpecialist(data.username, data.password).subscribe(() => {
      this.router.navigateByUrl("/");
    });
  }
  focusUsername(): string{
    if(this.nutritionistSigninForm.value.username)
      return 'focus';
    return 'blur';
  }
  focusPassword(): string{
    if(this.nutritionistSigninForm.value.password)
      return 'focus';
    return 'blur';
  }
}
