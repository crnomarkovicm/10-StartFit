import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionistsListComponent } from './nutritionists-list.component';

describe('NutritionistsListComponent', () => {
  let component: NutritionistsListComponent;
  let fixture: ComponentFixture<NutritionistsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NutritionistsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionistsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
