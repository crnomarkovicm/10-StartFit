import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { NutritionistService } from '../services/nutritionist.service.service';
import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nutritionists-list',
  templateUrl: './nutritionists-list.component.html',
  styleUrls: ['./nutritionists-list.component.css']
})
export class NutritionistsListComponent implements OnInit {


  @Input() nutritionist: Observable<HealthFitnessSpecialist[]>;

  nutritionistsObservable: Observable<HealthFitnessSpecialist[]>;
  nutritionists: HealthFitnessSpecialist[];
  page: number = 1;
  limit: number = 10;
  city: string;
  municipality: string;
  categories: [string];
  min: number;
  max: number;
  sort: string;

  constructor(private nutritionistService: NutritionistService, private activateRouter: ActivatedRoute) {
    this.activateRouter.queryParams.subscribe(
      params => {
        this.city = params.city;
        this.municipality = params.municipality;
        this.min = params.min;
        this.max = params.max;
        this.categories = params.categories;
        this.sort = params.sort;
      }
    );

    this.nutritionistsObservable = this.nutritionistService.getNutritionists(this.page, this.limit, this.min, this.max, this.city, this.municipality, this.categories, this.sort);

    const sub = this.nutritionistsObservable.subscribe((t : HealthFitnessSpecialist[])=> {
      this.nutritionists = t;
    });
  }

  ngOnInit() {

  }

  public next(): void {
    if(this.nutritionists.length == 10){
      this.page += 1;
      this.nutritionistsObservable = this.nutritionistService.getNutritionists(this.page, this.limit, this.min, this.max, this.city, this.municipality, this.categories, this.sort);
      const sub = this.nutritionistsObservable.subscribe((t : HealthFitnessSpecialist[])=> {
        this.nutritionists = t;
      });
    }
  }

  public previous(): void {
    if(this.page > 1){
      this.page -= 1;
      this.nutritionistsObservable = this.nutritionistService.getNutritionists(this.page, this.limit, this.min, this.max, this.city, this.municipality, this.categories, this.sort);
      const sub = this.nutritionistsObservable.subscribe((t : HealthFitnessSpecialist[])=> {
        this.nutritionists = t;
      });
    }
  }

}
