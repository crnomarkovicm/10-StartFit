import { TestBed } from '@angular/core/testing';

import { NutritionistAuthService } from './nutritionist-auth.service';

describe('NutritionistAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NutritionistAuthService = TestBed.get(NutritionistAuthService);
    expect(service).toBeTruthy();
  });
});
