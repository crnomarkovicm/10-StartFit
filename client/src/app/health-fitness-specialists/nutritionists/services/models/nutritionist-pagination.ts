import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';

export interface SpecialistData {
  _id: string;
  username: string;
  email: string;
  name: string;
  surname: string;
  gender: string;
  location: {
    city: string;
    municipality: string;
  }
  dateOfBirth: string;
  imgUrl: string,
  marks: number,
  numberOfCustomer: number,
  category: [string],
  score: number,
  approximatePrice: number
}

export interface NutritionistPagination {
  docs: SpecialistData[];
  "totalDocs": number,
  "limit": number,
  "totalPages": number,
  "page": number,
  "pagingCounter": number,
  "hasPrevPage": boolean,
  "hasNextPage": boolean,
  "prevPage": number | null,
  "nextPage": number | null
}
