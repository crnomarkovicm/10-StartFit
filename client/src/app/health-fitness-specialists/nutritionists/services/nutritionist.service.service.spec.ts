import { TestBed } from '@angular/core/testing';

import { Nutritionist.ServiceService } from './nutritionist.service.service';

describe('Nutritionist.ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Nutritionist.ServiceService = TestBed.get(Nutritionist.ServiceService);
    expect(service).toBeTruthy();
  });
});
