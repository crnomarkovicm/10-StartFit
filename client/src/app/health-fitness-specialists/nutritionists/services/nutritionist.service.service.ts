import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NutritionistPagination, SpecialistData } from './models/nutritionist-pagination';
import {Recipes} from 'src/app/profile/favorite-recipes/favorite-recipes.model'
import { JwtService } from 'src/app/common/services/jwt.service';
import { NutritionistAuthService } from './nutritionist-auth.service';
import { HealthFitnessSpecialist } from '../../models/healthFitnessSpecialist.model';
import { catchError, map } from 'rxjs/operators';
import { TokenId } from 'src/app/common/services/models/jwt-token-ids';
import { handleError } from 'src/app/common/server-error/server-error';

@Injectable({
  providedIn: 'root'
})
export class NutritionistService {

  constructor(private http: HttpClient, 
    private jwtService: JwtService,
    private auth: NutritionistAuthService) { }


  public patchSpecialistPassword(username: string, oldPassword: string, newPassword: string): Observable<HealthFitnessSpecialist> {
    const body = { username, oldPassword, newPassword };
    const patchSpecialistPasswordUrl = `http://localhost:3000/api/nutritionists/username/${username}`;
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken(this.auth.tokenId)}`);
    return this.http.patch<{ token: string }>(patchSpecialistPasswordUrl, body, { headers }).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService, TokenId.NUTRITIONIST_ID)),
      map((response: { token: string }) => this.auth.mapResponseToSpecialist(response)),
    );
  }

  public patchSpecialistProfileImage(file: File): Observable<HealthFitnessSpecialist> {
    const body: FormData = new FormData();
    body.append("file", file);
    const patchSpecialistImgUrl = `http://localhost:3000/api/nutritionists/profile-image/`;

    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken(this.auth.tokenId)}`);
    return this.http.patch<{ token: string }>(patchSpecialistImgUrl, body, { headers }).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService, TokenId.NUTRITIONIST_ID)),
      map((response: { token: string }) => this.auth.mapResponseToSpecialist(response)),
    );
  }

  public getNutritionistsRecipes(username: string): Observable<Recipes[]>{
    return this.http.get<Recipes[]>(`http://localhost:3000/api/nutritionists/username/${username}/recipes`);
  }

  public getNutritionists(page: number = 1, limit: number = 10, min: number, max:number, city: string, municipality: string, category:[string], sort: string): Observable<HealthFitnessSpecialist[]> {
    const params: HttpParams = new HttpParams()
    .append('page', page.toString())
    .append('limit', limit.toString());
    const body = {min, max, city, municipality, category, sort }
    console.log(body);
    const obs: Observable<NutritionistPagination> = this.http.post<NutritionistPagination>(
      "http://localhost:3000/api/nutritionists",
      body,
      {
        params
      }
    );

    const newObs: Observable<HealthFitnessSpecialist[]> = obs.pipe(
      map((pagination: NutritionistPagination) => {
        return pagination.docs.map (
          (doc : SpecialistData) =>
          new HealthFitnessSpecialist(
              doc._id,
              doc.username,
              doc.name,
              doc.surname,
              doc.email,
              doc.gender,
              doc.location.city,
              doc.location.municipality,
              doc.dateOfBirth,
              doc.imgUrl,
              doc.score,
              doc.category,
              doc.approximatePrice,
              doc.numberOfCustomer,
              doc.marks
            )

        );
      })
    );
    console.log(newObs);

    return newObs;
  }

  public postNutritionist(nutritionist_id: string, user_username : string){
    this.http.put<any>(`http://localhost:3000/api/customers/username/${user_username}/addNutritionist/${nutritionist_id}`, "").subscribe(data => console.log(data));
  }

  public rateNutritionist(nutritionist_username: string, mark: number){
    this.http.put<any>(`http://localhost:3000/api/customers/rateNutritionist/${nutritionist_username}`, {mark}).subscribe(data => console.log(data));
  }
}
