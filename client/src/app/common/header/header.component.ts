import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { User } from "src/app/regular-users/models/user.model";
import { AuthUserService } from "src/app/regular-users/services/auth-reg-user.service";
import { Subscription } from 'rxjs';
import { HealthFitnessSpecialist } from "src/app/health-fitness-specialists/models/healthFitnessSpecialist.model";
import { Router } from '@angular/router';
import { TrainerAuthService } from 'src/app/health-fitness-specialists/trainers/services/trainer-auth.service';
import { NutritionistAuthService } from 'src/app/health-fitness-specialists/nutritionists/services/nutritionist-auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnDestroy{

  private user: User = null;
  private userSub: Subscription;
  private trainer: HealthFitnessSpecialist = null;
  private trainerSub: Subscription;
  private nutritionist: HealthFitnessSpecialist = null;
  private nutritionistSub: Subscription;

  
  constructor(private authUserService: AuthUserService, 
    private authTrainerService: TrainerAuthService,
    private authNutritionistService: NutritionistAuthService,
    private router: Router) {

      //user
      this.userSub = this.authUserService.user.subscribe((user: User) => {
        this.user = user;
        
      });
      this.authUserService.sendUserDataIfExists();
      
    
      //trainer
      this.trainerSub = this.authTrainerService.specialist.subscribe((specialist: HealthFitnessSpecialist) => {
        this.trainer = specialist;
      });
      this.authTrainerService.sendSpecialistDataIfExists();

      //nutritionist
      this.nutritionistSub = this.authNutritionistService.specialist.subscribe((specialist: HealthFitnessSpecialist) => {
        this.nutritionist = specialist;
      });
      this.authNutritionistService.sendSpecialistDataIfExists();


  }

  private get homeIsActive(): boolean {
    return window.location.href === 'http://localhost:4200/';
  }


  ngOnDestroy() {
    this.userSub ? this.userSub.unsubscribe() : null;
    this.trainerSub ? this.trainerSub.unsubscribe() : null;
    this.nutritionistSub ? this.nutritionistSub.unsubscribe() : null;
  }

  private get profilePic(): string {
    if (this.authUserService.userLoggedIn)
      return this.user.imgUrl;
    else if (this.authNutritionistService.specialistLoggedIn)
      return this.nutritionist.imgUrl;
    else if (this.authTrainerService.specialistLoggedIn)
      return this.trainer.imgUrl;
  }

  private get isSomeoneLoggedIn() : boolean {
    return (this.authUserService.userLoggedIn || this.authTrainerService.specialistLoggedIn || this.authNutritionistService.specialistLoggedIn );
  }


  public logout(): void {

    if (this.authUserService.userLoggedIn) {
      this.authUserService.logoutUser();
    }
    else if (this.authTrainerService.specialistLoggedIn) {
      this.authTrainerService.logoutSpecialist();
    }
    else if (this.authNutritionistService.specialistLoggedIn) {
      this.authNutritionistService.logoutSpecialist();
    }
    this.router.navigateByUrl("/");
  
  }
  
}
