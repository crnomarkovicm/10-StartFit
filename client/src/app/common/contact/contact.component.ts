import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from '../services/contact.service';
declare const $: any;

interface IContactFormValue {
  name: string;
  email: string;
  subject: string;
  message: string;
}


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  public checkoutForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private contactService: ContactService) {
    this.checkoutForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['',Validators.required],
      subject: [''],
      message: ['', Validators.required]
    });

   }

  ngOnInit() {
    $('.ui.modal')
      .modal();
  }

  onSubmitForm(): void {
    // Provera da li je formular validan
    if (!this.checkoutForm.valid) {
      $('.ui.modal')
      .modal('show');
    
      return;
    }
    
    const data = this.checkoutForm.value as IContactFormValue;
    console.log(data);
 
    this.contactService.sendMail(this.checkoutForm.value).subscribe((data)=>{
     console.log(data);
     window.alert('Thanks for your message ' + this.checkoutForm.value.name);
     
      this.checkoutForm.reset({
        name: "",
        email: "",
        subject: "",
        message: ""
      });
    });

   
  }

  focusName(): string{
    if(this.checkoutForm.value.name)
      return 'focus';
    return 'blur';
  }
  focusEmail(): string{
    if(this.checkoutForm.value.email)
      return 'focus';
    return 'blur';
  }
  focusMessage(): string{
    if(this.checkoutForm.value.message)
      return 'focus';
    return 'blur';
  }
  focusSubject(): string{
    if(this.checkoutForm.value.subject)
      return 'focus';
    return 'blur';
  }
}


