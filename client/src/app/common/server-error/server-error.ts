import { HttpErrorResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { JwtService } from "../services/jwt.service";
import { TokenId } from "../services/models/jwt-token-ids";


export function handleError(error: HttpErrorResponse, jwtService: JwtService, tokenId: TokenId): Observable<{ token: string }> {

    const serverError: { message: string; status: number; stack: string } = error.error;
    window.alert(`Server error: ${serverError.message} (${serverError.status})`);
    if (error != null)
      return;
    return of({ token: jwtService.getToken(tokenId) });
  }