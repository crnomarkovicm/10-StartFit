import { Injectable } from '@angular/core';
import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';
import { User } from 'src/app/regular-users/models/user.model';
import { IJWTTokenData, IJWTHealthFitnessSpecialistTokenData } from './models/jwt-token-data';
import { TokenId } from './models/jwt-token-ids';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor() {}

  public getToken(tokenId: TokenId): string | null {
    const token: string | null = localStorage.getItem(tokenId);
    if (!token) {
      return null;
    }
    return token;
  }

  public getUserDataFromToken(tokenId: TokenId): IJWTTokenData {
    const token = this.getToken(tokenId);

    if (!token) {
      return null;
    }

    const payloadString = token.split(".")[1];
    const jsonData = window.atob(payloadString);
      var payload : IJWTTokenData = JSON.parse(jsonData);
      return payload;
  }

  public getHealthFitnessSpecialistDataFromToken(tokenId: TokenId): IJWTHealthFitnessSpecialistTokenData{
    const token = this.getToken(tokenId);

    if (!token) {
      return null;
    }

    const payloadString = token.split(".")[1];
    const jsonData = window.atob(payloadString);
    const payload: IJWTHealthFitnessSpecialistTokenData = JSON.parse(jsonData);
    return payload;
  }

  public setToken(jwt: string, tokenId: TokenId): void {
    localStorage.setItem(tokenId, jwt);
  }

  public removeToken(tokenId: TokenId): void {
    localStorage.removeItem(tokenId);
  }
}
