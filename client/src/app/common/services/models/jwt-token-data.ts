export interface IJWTTokenData {
  id: string;
  username: string;
  email: string;
  name: string;
  surname: string;
  gender: string;
  city: string;
  municipality: string;
  dateOfBirth: string;
  imgUrl: string,
  marks: number,
  numberOfCustomer: number
}

export interface IJWTHealthFitnessSpecialistTokenData extends IJWTTokenData{
  category: [string],
  score: number,
  approximatePrice: number
}
