import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtService } from './jwt.service';
import { TokenId } from './models/jwt-token-ids';

@Injectable({
  providedIn: 'root'
})
export class DeleteService {

  constructor(private http: HttpClient, private jwtService: JwtService) { }

  public deleteRecipe(username: string, id: string, type: string) {
     return this.http.put(`http://localhost:3000/api/${type}/username/${username}/recipes/${id}`, {});
  }

  public deleteProfile(username: string, type: string) {

    let tokenId: TokenId;
    if (type == "customers")
      tokenId = TokenId.USER_ID;
    else if( type == "trainers")
      tokenId = TokenId.TRAINER_ID;
    else
      tokenId = TokenId.NUTRITIONIST_ID;

    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken(tokenId)}`);

    return this.http.delete(`http://localhost:3000/api/${type}/username/${username}`, {headers});
  }
  public deleteTrainer(username: string, id: string){
    return this.http.put(`http://localhost:3000/api/customers/username/${username}/deleteTrainer/${id}`, {});
  }
  public deleteNutritionist(username: string, id: string){
    console.log(id);
    return this.http.put(`http://localhost:3000/api/customers/username/${username}/deleteNutritionist/${id}`, {});
  }
}
