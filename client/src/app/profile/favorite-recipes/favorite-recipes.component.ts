import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { User } from "src/app/regular-users/models/user.model";
import { Subscription, Observable } from 'rxjs';
import { HealthFitnessSpecialist } from "src/app/health-fitness-specialists/models/healthFitnessSpecialist.model";
import { RegUserService } from 'src/app/regular-users/services/reg-user.service';
import {Recipes} from 'src/app/profile/favorite-recipes/favorite-recipes.model'
import { DeleteService } from 'src/app/common/services/delete.service';
import { TrainerService } from 'src/app/health-fitness-specialists/trainers/services/trainer.service';
import { NutritionistService } from 'src/app/health-fitness-specialists/nutritionists/services/nutritionist.service.service';

@Component({
  selector: 'app-favorite-recipes',
  templateUrl: './favorite-recipes.component.html',
  styleUrls: ['./favorite-recipes.component.css']
})
export class FavoriteRecipesComponent implements OnInit {

  @Input('user')
  public user: User;
  @Input('nutritionist')
  public nutritionist: HealthFitnessSpecialist;
  @Input('trainer')
  public trainer: HealthFitnessSpecialist;

  public recipes: Observable<Recipes[]>;
  public customer: Observable<User>;
  public trainers: Observable<HealthFitnessSpecialist>;

  constructor(private userService: RegUserService, 
    private trainerService: TrainerService,
    private nutritionistService: NutritionistService,
    private deleteServices: DeleteService) { }

    private get username(): string {

      if(this.userIn)
        return this.user.username;
      else if(this.trainerIn)
        return this.trainer.username;
      else if(this.nutritionistIn)
        return this.nutritionist.username   
    }
  
    private get getRecipes(): Observable<Recipes[]> {

      if(this.userIn)
         return this.userService.getCustomersRecipes(this.username);
      else if(this.trainerIn)
         return this.trainerService.getTrainersRecipes(this.username);
      else if(this.nutritionistIn)
         return this.nutritionistService.getNutritionistsRecipes(this.username);
    }
  
    ngOnInit() {
      this.recipes = this.getRecipes;
      

    }

  private get userIn(): boolean {
    return this.user !== null;
  }

  private get nutritionistIn(): boolean {
    return this.nutritionist !== null;
  }

  private get trainerIn(): boolean {
    return this.trainer !== null;
  }

  
  deleteRecipe(id: string) {

    console.log(id);
    if(this.userIn)
      this.deleteServices.deleteRecipe(this.username, id,"customers").subscribe((data)=>{
      console.log(data);
      this.recipes = this.getRecipes;
     });
    else if(this.trainerIn)
      this.deleteServices.deleteRecipe(this.username, id,'trainers').subscribe((data)=>{
      console.log(data);
      this.recipes = this.getRecipes;
     });
    else
    this.deleteServices.deleteRecipe(this.username, id,'nutritionists').subscribe((data)=>{
      console.log(data);
      this.recipes = this.getRecipes;
     });
    
  }
  getColorForCategory(indeks):string{
    return indeks? "#d96846": "#013328";
  }
     
  

  
}
