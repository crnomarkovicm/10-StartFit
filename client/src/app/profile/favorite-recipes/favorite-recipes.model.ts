import { environment } from "src/environments/environment";

export class Recipes {
    constructor(
      public idRecipe: string,
      public name : string,
      public summary: string,
      public ingredients: string[],
      public instructions: string,
      public prepTime: number,
      public score: number,
      public dishTypes: string[],
      public image: string
    ){}

   
}    
  