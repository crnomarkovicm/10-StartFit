import { Component, OnDestroy, OnInit } from '@angular/core'
import { User } from "src/app/regular-users/models/user.model";
import { AuthUserService } from "src/app/regular-users/services/auth-reg-user.service";
import { Subscription } from 'rxjs';
import { HealthFitnessSpecialist } from "src/app/health-fitness-specialists/models/healthFitnessSpecialist.model";
import { TrainerAuthService } from '../health-fitness-specialists/trainers/services/trainer-auth.service';
import { NutritionistAuthService } from '../health-fitness-specialists/nutritionists/services/nutritionist-auth.service';
import { Router } from '@angular/router';
declare const $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


  private user: User = null;
  private userSub: Subscription;
  private trainer: HealthFitnessSpecialist = null;
  private trainerSub: Subscription;
  private nutritionist: HealthFitnessSpecialist = null;
  private nutritionistSub: Subscription;


  constructor(private authUserService: AuthUserService,
    private authTrainerService: TrainerAuthService,
    private authNutritionistService: NutritionistAuthService) {
      //user
      this.userSub = this.authUserService.user.subscribe((user: User) => {
        this.user = user;

      });
      this.authUserService.sendUserDataIfExists();

      //trainer
      this.trainerSub = this.authTrainerService.specialist.subscribe((specialist: HealthFitnessSpecialist) => {
        this.trainer = specialist;
      });
      this.authTrainerService.sendSpecialistDataIfExists();

      //nutritionist
      this.nutritionistSub = this.authNutritionistService.specialist.subscribe((specialist: HealthFitnessSpecialist) => {
        this.nutritionist = specialist;
      });
      this.authNutritionistService.sendSpecialistDataIfExists();

    }

  ngOnInit() {
    $('.menu .item').tab();
  }

  ngOnDestroy() {
    this.userSub ? this.userSub.unsubscribe() : null;
    this.trainerSub ? this.trainerSub.unsubscribe() : null;
    this.nutritionistSub ? this.nutritionistSub.unsubscribe() : null;
  }

  private get isUserLoggedIn(): boolean {
    $('.menu .item').tab();
    return this.authUserService.userLoggedIn;
  }

}
