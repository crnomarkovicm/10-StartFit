import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { User } from "src/app/regular-users/models/user.model";
import { Subscription, Observable } from 'rxjs';
import { RegUserService } from 'src/app/regular-users/services/reg-user.service';
import { HealthFitnessSpecialist } from "src/app/health-fitness-specialists/models/healthFitnessSpecialist.model";
import { DeleteService } from 'src/app/common/services/delete.service';

@Component({
  selector: 'app-favorite-trainers',
  templateUrl: './favorite-trainers.component.html',
  styleUrls: ['./favorite-trainers.component.css']
})
export class FavoriteTrainersComponent implements OnInit {

  @Input('user') 
  public user: User;
 

  public trainers: Observable<HealthFitnessSpecialist[]>;
  trainerSub: Subscription;

  constructor(private userService: RegUserService, private deleteService: DeleteService){}

  private get username(): string {
    return this.user.username;
  }

  private get getTrainers(): Observable<HealthFitnessSpecialist[]> {
    console.log(this.username);
    return this.userService.getTrainers(this.username);
  }


  ngOnInit(){
    this.trainers = this.getTrainers;
  }
  getColorForCategory(indeks):string{
    return indeks? "#d96846": "#013328";
  }

  deleteTrainer(id: any){
    this.deleteService.deleteTrainer(this.username,id).subscribe((data)=>{
      console.log(data);
      this.trainers = this.getTrainers;
     });
  }

}
