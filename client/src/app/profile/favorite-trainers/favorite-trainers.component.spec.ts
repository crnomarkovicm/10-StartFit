import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteTrainersComponent } from './favorite-trainers.component';

describe('FavoriteTrainersComponent', () => {
  let component: FavoriteTrainersComponent;
  let fixture: ComponentFixture<FavoriteTrainersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteTrainersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteTrainersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
