import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteNutritionistsComponent } from './favorite-nutritionists.component';

describe('FavoriteNutritionistsComponent', () => {
  let component: FavoriteNutritionistsComponent;
  let fixture: ComponentFixture<FavoriteNutritionistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteNutritionistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteNutritionistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
