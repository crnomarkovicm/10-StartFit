import { Component, OnInit, Input } from '@angular/core';
import { HealthFitnessSpecialist } from "src/app/health-fitness-specialists/models/healthFitnessSpecialist.model";
import { Subscription, Observable } from 'rxjs';
import { RegUserService } from 'src/app/regular-users/services/reg-user.service';
import { User } from "src/app/regular-users/models/user.model";
import { DeleteService } from 'src/app/common/services/delete.service';

@Component({
  selector: 'app-favorite-nutritionists',
  templateUrl: './favorite-nutritionists.component.html',
  styleUrls: ['./favorite-nutritionists.component.css']
})
export class FavoriteNutritionistsComponent implements OnInit {

  @Input('user')
  public user: User;

  public nutritionists: Observable<HealthFitnessSpecialist[]>;
  nutritionistSub: Subscription;

  constructor(private userService: RegUserService, private deleteService: DeleteService) { }

  private get username(): string {
    return this.user.username;
  }

  private get getNutritionists(): Observable<HealthFitnessSpecialist[]> {
    console.log(this.username);
    return this.userService.getNutritionists(this.username);
  }

  ngOnInit() {
    this.nutritionists = this.getNutritionists;
  }

  deleteNutritionist(id: any){
    console.log(id);
    this.deleteService.deleteNutritionist(this.username,id).subscribe((data)=>{
      console.log(data);
      this.nutritionists = this.getNutritionists;
     });
  }
  getColorForCategory(indeks):string{
    return indeks? "#d96846": "#013328";
  }

}
