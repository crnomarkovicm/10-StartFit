import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindTrainerComponent } from './find-trainer.component';

describe('FindTrainerComponent', () => {
  let component: FindTrainerComponent;
  let fixture: ComponentFixture<FindTrainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindTrainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindTrainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
