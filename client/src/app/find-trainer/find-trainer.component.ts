import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {TrainerCategory} from '../health-fitness-specialists/models/healthFitnessSpecialistCategories'
import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';
import { Observable } from 'rxjs';
import { TrainerService } from '../health-fitness-specialists/trainers/services/trainer.service';
import { Router, NavigationExtras } from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-find-trainer',
  templateUrl: './find-trainer.component.html',
  styleUrls: ['./find-trainer.component.css']
})
export class FindTrainerComponent implements OnInit {

  categories: Array<any> = Object.values(TrainerCategory);
  cat: [string];

  findTrainerForm: FormGroup;
  trainers: Observable<HealthFitnessSpecialist[]>;
  selectedCategories: [string];
  sort: string;

  get categoriesFormArray() {
    return this.findTrainerForm.controls.categories as FormArray;
  }

  constructor(private formBuilder:FormBuilder, private trainerService: TrainerService, private router: Router) {

    this.findTrainerForm = formBuilder.group({
      minPrice: new FormControl("", []),
      maxPrice: new FormControl("", []),
      city: new FormControl("", []),
      municipality: new FormControl("", []),
      categories: new FormArray([]),
      sort: new FormControl(false, [])
    });
    this.addCheckboxes();
   }

  ngOnInit() {
    $('.menu .item').tab();
  }

  public findTrainer(): void {}

  private addCheckboxes() {
    this.categories.forEach(() => this.categoriesFormArray.push(new FormControl(false)));
  }

  submit() {
  /* ovo je niz svih kategorija koje je korisnik odabrao*/


    this.selectedCategories = this.findTrainerForm.value.categories
        .map((checked, i) => checked ? this.categories[i] : null)
        .filter(v => v !== null);

        this.sort="";
        if(this.findTrainerForm.value.sort)
          this.sort = "score";


    const length: number = this.selectedCategories.length;

    if (length > 0) {
      this.router.navigate(['/trainers/trainers-list'], {
        queryParams: {
          min:this.findTrainerForm.value.minPrice,
          max:this.findTrainerForm.value.maxPrice,
          city:this.findTrainerForm.value.city,
          municipality:this.findTrainerForm.value.municipality,
          categories:this.selectedCategories,
          sort:this.sort
        }
      });
    }
    else {
      this.router.navigate(['/trainers/trainers-list'], {
        queryParams: {
          min:this.findTrainerForm.value.minPrice,
          max:this.findTrainerForm.value.maxPrice,
          city:this.findTrainerForm.value.city,
          municipality:this.findTrainerForm.value.municipality,
          categories:this.cat,
          sort:this.sort
        }
      });
    }
  }

}
