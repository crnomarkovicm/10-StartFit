import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './common/home/home.component';
import { ContactComponent } from './common/contact/contact.component';
import { FindRecipeComponent } from './find-recipe/find-recipe.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { AboutComponent } from './common/about/about.component';
import { FindTrainerComponent } from './find-trainer/find-trainer.component';
import { FindNutritionistComponent } from './find-nutritionist/find-nutritionist.component';
import { TrainersListComponent } from './health-fitness-specialists/trainers/trainers-list/trainers-list.component';
import { RecipeListComponent } from './find-recipe/recipe-list/recipe-list.component';
import { NutritionistsListComponent } from './health-fitness-specialists/nutritionists/nutritionists-list/nutritionists-list.component';
import { NutritionistInfoComponent } from './health-fitness-specialists/nutritionists/nutritionist-info/nutritionist-info.component';
import { ProfileComponent } from './profile/profile.component';
import { UserAuthenticatedGuard } from './guards/user-authenticated.guard';
import { UserLoggedInGuard } from './guards/user-logged-in.guard';
import { RecipeInfoComponent } from './find-recipe/recipe-info/recipe-info.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'signin', component: SigninComponent, canActivate: [UserLoggedInGuard]},
  {path: 'signup', component: SignupComponent, canActivate: [UserLoggedInGuard]},
  {path: 'contact', component: ContactComponent},
  {path: 'find-recipe', component: FindRecipeComponent},
  {path: 'edit-profile', component: EditProfileComponent},
  {path: 'about', component: AboutComponent},
  {path: 'find-trainer', component: FindTrainerComponent},
  {path: 'find-nutritionist', component: FindNutritionistComponent},
  {path: 'trainers/trainers-list', component: TrainersListComponent},
  {path: 'recipe-list', component: RecipeListComponent},
  {path: 'nutritionists/nutritionists-list', component: NutritionistsListComponent},
  {path: 'nutritionists/nutritionist-info', component: NutritionistInfoComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [UserAuthenticatedGuard]},
  {path: 'recipe-info', component: RecipeInfoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HomeComponent, SigninComponent, SignupComponent, ContactComponent, FindRecipeComponent, ProfileComponent, AboutComponent, FindTrainerComponent, FindNutritionistComponent, RecipeListComponent]
