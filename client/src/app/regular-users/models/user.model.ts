import { environment } from "src/environments/environment";

export class User {
    //constructor(public id: string,public username: string, public email: string, public name: string, public imgUrl: string) {}

    constructor(public id: string,public username: string,
      public name: string, public surname: string,
      public email: string, public gender: string,
      public city: string, public municipality: string,
      public dateOfBirth: string,
      public _imgUrl: string) {}

      public get imgUrl(): string {
        return `${environment.fileDownloadUrl}/${this._imgUrl}`;
      }
  }
