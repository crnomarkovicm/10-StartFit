import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { AuthUserService } from '../services/auth-reg-user.service';

@Component({
  selector: 'app-signup-regular-user',
  templateUrl: './signup-regular-user.component.html',
  styleUrls: ['./signup-regular-user.component.css']
})
export class SignupRegularUserComponent implements OnInit, OnDestroy {

  somethingIsNotValidUser: boolean = false;
  allIsValidUser: boolean = true;
  userForm: FormGroup;
  userSub: Subscription;
  passwordNotConfirmed: boolean = false;

  constructor(private authService: AuthUserService,
              private router: Router) {

    this.userForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(2)]),
      surname: new FormControl("", [Validators.required, Validators.minLength(2)]),
      username: new FormControl("", [Validators.required, Validators.minLength(2)]),
      email: new FormControl("", [Validators.required, Validators.email]),
      city: new FormControl("", [Validators.required]),
      municipality: new FormControl("", [Validators.required]),
      dateOfBirth: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required, Validators.minLength(8)]),
      cnfPassword: new FormControl("", [Validators.required, Validators.minLength(8)]),
      gender: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {

  }
  ngOnDestroy() {

    if(this.userSub)
      this.userSub.unsubscribe();
  }



  public register(): void {

    this.checkPasswords(this.userForm);

    if(this.userForm.invalid || this.passwordNotConfirmed) {
      this.somethingIsNotValidUser = true;
      this.allIsValidUser = false;
      window.alert('Form is not valid. Please try again!');
      return;
    }

    this.somethingIsNotValidUser = false;
    this.allIsValidUser = true;

    console.log(this.userForm.value);
    var img: string;

      if (this.userForm.value.gender == 'male')
        img = 'male_avatar.png';
      else if (this.userForm.value.gender == 'female')
        img = 'female_avatar.png'
      else
        img = 'other_avatar.png'


    const obsUser: Observable<User> =  this.authService.registerUser(
      this.userForm.value.username,
      this.userForm.value.password,
      this.userForm.value.name,
      this.userForm.value.surname,
      this.userForm.value.email,
      this.userForm.value.gender,
      this.userForm.value.city,
      this.userForm.value.municipality,
      this.userForm.value.dateOfBirth,
      img
      );


      //if (this.userSub)
        //this.userSub.unsubscribe();
      this.userSub = obsUser.subscribe( () => {
          this.router.navigateByUrl("/");
        }
      );
  }

  checkPasswords(group: FormGroup) {
    const password = group.get('password').value;
    const cnfPassword = group.get('cnfPassword').value;
    if (password === cnfPassword) {
      this.passwordNotConfirmed = false;
    }
    else {
      this.passwordNotConfirmed = true;
    }
  }

  public nameHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('name').errors;

    return errors !== null;
  }

  public nameErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('name').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('User must have a first name.');
    }

    return errorMessages;
  }

  surnameHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('surname').errors;

    return errors !== null;
  }

  usernameHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('username').errors;

    return errors !== null;
  }

  municipalityHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('municipality').errors;

    return errors !== null;
  }

  cityHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('city').errors;

    return errors !== null;
  }

  emailHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('email').errors;

    return errors !== null;
  }

  dateOfBirthHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('dateOfBirth').errors;

    return errors !== null;
  }

  passwordHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('password').errors;

    return errors !== null;
  }

  cnfPasswordHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('cnfPassword').errors;

    return errors !== null;
  }

  genderHasErrors(): boolean {
    const errors: ValidationErrors = this.userForm.get('gender').errors;

    return errors !== null;
  }

  public surnameErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('surname').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('User must have a last name.');
    }

    return errorMessages;
  }

  public usernameErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('username').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('User must have a username.');
    }

    return errorMessages;
  }

  public emailErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('email').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('User must have an email.');
    }
    if (errors.email) {
      errorMessages.push('Users email must have a valid email format.');
    }

    return errorMessages;
  }

  public municipalityErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('municipality').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a municipality.');
    }

    return errorMessages;
  }

  public cityErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('city').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Nutritionist must have a city.');
    }

    return errorMessages;
  }

  public dateOfBirthErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('dateOfBirth').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('User must have a date of birth.');
    }

    return errorMessages;
  }

  public passwordErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('password').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('User must have a first name.');
    }

    return errorMessages;
  }

  public cnfPasswordErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('cnfPassword').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Password must be confirmed.');
    }

    return errorMessages;
  }

  public genderErrors(): string[] {
    const errors: ValidationErrors = this.userForm.get('gender').errors;
    if(errors === null) {
      return [];
    }

    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('Gender must be choosen.');
    }

    return errorMessages;
  }
}
