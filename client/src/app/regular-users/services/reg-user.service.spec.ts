import { TestBed } from '@angular/core/testing';

import { RegUserService } from './reg-user.service';

describe('RegUserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegUserService = TestBed.get(RegUserService);
    expect(service).toBeTruthy();
  });
});
