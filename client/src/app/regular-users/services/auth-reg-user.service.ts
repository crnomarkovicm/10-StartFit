import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { User } from '../models/user.model';
import { catchError, map, tap } from 'rxjs/operators';
import { JwtService } from 'src/app/common/services/jwt.service';
import { IJWTTokenData } from 'src/app/common/services/models/jwt-token-data';
import { TokenId } from 'src/app/common/services/models/jwt-token-ids';
import { handleError } from 'src/app/common/server-error/server-error';


@Injectable({
  providedIn: 'root'
})
export class AuthUserService {

  private readonly userSubject: Subject<User> = new Subject<User>();
  public readonly user: Observable<User> = this.userSubject.asObservable();
  private isUserLoggedIn: boolean = false;

  private readonly urls = {
    registerUser: "http://localhost:3000/api/customers/register/",
    loginUser: "http://localhost:3000/api/customers/login/"
  };

  constructor(private http: HttpClient, private jwtService: JwtService) {}

  public sendUserDataIfExists(): User {
    const payload: IJWTTokenData = this.jwtService.getUserDataFromToken(TokenId.USER_ID);
    const user: User = payload
      ? new User(payload.id, payload.username, payload.name, payload.surname,
        payload.email, payload.gender, payload.city, payload.municipality,
        payload.dateOfBirth, payload.imgUrl)
      : null;
    this.userSubject.next(user);

    this.isUserLoggedIn = user !== null;
    
    return user;
  }

  public get userLoggedIn(): boolean {
    return this.isUserLoggedIn;
  }

  public set setUserLoggedIn(isLogged: boolean) {
    this.isUserLoggedIn = isLogged;
  }

  public registerUser(username: string, password: string, name: string, surname: string,
    email: string, gender: string, city: string, municipality: string, dateOfBirth: string, imgUrl: string): Observable<User> {

    const body = { username, name, surname, email, password, gender, city, municipality, dateOfBirth, imgUrl };
    return this.http.post<{ token: string }>(this.urls.registerUser, body).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService, TokenId.USER_ID)),
      map((response: { token: string }) => this.mapResponseToUser(response))
    );
  }

  public signInUser(username: string, password: string): Observable<User> {
    const body = { username, password };
    return this.http.post<{ token: string }>(this.urls.loginUser, body).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService, TokenId.USER_ID)),
      map((response: { token: string }) => this.mapResponseToUser(response))
    );
  }

  public logoutUser(): void {
    this.jwtService.removeToken(TokenId.USER_ID);
    this.isUserLoggedIn = false;
    this.userSubject.next(null);
  }

  private mapResponseToUser(response: { token: string }): User {
    this.jwtService.setToken(response.token, TokenId.USER_ID);
    return this.sendUserDataIfExists();
  }

}
