import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, observeOn } from 'rxjs/operators';
import { JwtService } from 'src/app/common/services/jwt.service';
import { TokenId } from 'src/app/common/services/models/jwt-token-ids';
import { User } from '../models/user.model';
import { AuthUserService } from './auth-reg-user.service';
import { HealthFitnessSpecialist } from 'src/app/health-fitness-specialists/models/healthFitnessSpecialist.model';
import {Recipes} from 'src/app/profile/favorite-recipes/favorite-recipes.model'

export interface SpecialistData {
  _id: string;
  username: string;
  email: string;
  name: string;
  surname: string;
  gender: string;
  location: {
    city: string;
    municipality: string;
  }
  dateOfBirth: string;
  imgUrl: string,
  marks: number,
  numberOfCustomer: number,
  category: [string],
  score: number,
  approximatePrice: number
}

@Injectable({
  providedIn: 'root'
})
export class RegUserService {

  private readonly patchUserImgUrl = 'http://localhost:3000/api/customers/profile-image/';

  constructor(private http: HttpClient, private jwtService: JwtService, private auth: AuthUserService) { }


  public patchUserProfileImage(file: File): Observable<User> {
    const body: FormData = new FormData();
    body.append("file", file);

    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken(TokenId.USER_ID)}`);
    return this.http.patch<{ token: string }>(this.patchUserImgUrl, body, { headers }).pipe(
      catchError((error: HttpErrorResponse) => this.handleError(error)),
      map((response: { token: string }) => this.mapResponseToUser(response)),
    );
  }


  public patchUserPassword(username: string, oldPassword: string, newPassword: string): Observable<User> {
    const body = { username, oldPassword, newPassword };
    const patchUserPasswordUrl = `http://localhost:3000/api/customers/username/${username}`;
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken(TokenId.USER_ID)}`);
    return this.http.patch<{ token: string }>(patchUserPasswordUrl, body, { headers }).pipe(
      catchError((error: HttpErrorResponse) => this.handleError(error)),
      map((response: { token: string }) => this.mapResponseToUser(response)),
    );
  }


  public getTrainers(username: string): Observable<HealthFitnessSpecialist[]>{

    const obs: Observable<HealthFitnessSpecialist[]>  =
    this.http.get<SpecialistData[]>(`http://localhost:3000/api/customers/username/${username}/trainers`)
    .pipe(
      map((trs: (SpecialistData[])) => {
        return trs.map(
          (tr: SpecialistData) => {
            return new HealthFitnessSpecialist(tr._id, tr.username, tr.name, tr.surname, tr.email,
              tr.gender, tr.location.city, tr.location.municipality, tr.dateOfBirth, tr.imgUrl, tr.score, tr.category, tr.approximatePrice,
              tr.numberOfCustomer, tr.marks)
          }
        )
      })
    );

    return obs;

  }

  public getNutritionists(username: string): Observable<HealthFitnessSpecialist[]>{

    const obs: Observable<HealthFitnessSpecialist[]>  =
    this.http.get<SpecialistData[]>(`http://localhost:3000/api/customers/username/${username}/nutritionists`)
    .pipe(
      map((nutrs: (SpecialistData[])) => {
        return nutrs.map(
          (nutr: SpecialistData) => {
            return new HealthFitnessSpecialist(nutr._id, nutr.username, nutr.name, nutr.surname, nutr.email,
              nutr.gender, nutr.location.city, nutr.location.municipality, nutr.dateOfBirth, nutr.imgUrl, nutr.score, nutr.category, nutr.approximatePrice,
              nutr.numberOfCustomer, nutr.marks)
          }
        )
      })
    );

    return obs;
  }

  public getCustomersRecipes(username: string): Observable<Recipes[]>{
    return this.http.get<Recipes[]>(`http://localhost:3000/api/customers/username/${username}/recipes`);
  }


  private handleError(error: HttpErrorResponse): Observable<{ token: string }> {
    const serverError: { message: string; status: number; stack: string } = error.error;
    window.alert(`There was an error: ${serverError.message}. Server returned code: ${serverError.status}`);
    if (error !== null)
        return;
    return of({ token: this.jwtService.getToken(TokenId.USER_ID) });
  }

  private mapResponseToUser(response: { token: string }): User {
    this.jwtService.setToken(response.token, TokenId.USER_ID);
    return this.auth.sendUserDataIfExists();
  }
}
