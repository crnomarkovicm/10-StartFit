const jwt = require('jsonwebtoken');
require('dotenv').config();

const jwtSecret =  process.env.JWT_SECRET;

const verifyToken = (req, res, next) => {
  var token = req.headers.authorization.split(' ')[1];

  if (!token) {
    return res.status(403).json({ auth: false, message: 'No token provided' });
  }

  jwt.verify(token, jwtSecret, (error, payloadData) => {
    if (error) {
      return res.status(500).json({ auth: false, message: 'Failed to authenticate token' });
    }
    req.username = payloadData.username;
    next();
  });
};

const generateToken = (data) => {
  return jwt.sign(data, jwtSecret, {expiresIn:'7d'});
};

module.exports = {
  verifyToken,
  generateToken
};
