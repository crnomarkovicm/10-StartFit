const nutritionistsService = require('../services/nutritionistsService')
const Nutritionist = require('../models/nutritionistModel');
const validator = require('validator');
const bcrypt = require('bcrypt');
const jwtAuth = require('../utils/authentication');
const { uploadFile } = require('../controllers/uploadController');
const Recipe = require('../models/recipeModel');

const getAllNutritionists = async (req, res, next) => {
    const page = req.query.page;
    const limit = req.query.limit;
    let filter ={};
    const options = req.body;
    console.log(options)

    if(options.location){
        filter.location={}
        if(options.location.municipality)
            filter.location = options.location;
        else
            filter = {
                "location.city": options.location.city
            }
    }
    if(options.min){
        filter.approximatePrice={$gte:options.min};
    }
    if(options.max){
        filter.approximatePrice["$lte"] = options.max;
    }
       
    if(options.category)
        filter.category = {$in: options.category}
  
    
    try {
        const nutritionist= await paginateThroughProducts(page,limit,filter,options.sort);
        res.status(200).json(nutritionist);
    } catch (err) {
        next(err);
    }
};

const getNutritionistByUsername = async (req, res, next) => {


    const username = req.params.username;
    try {
        if (!username) {
            const error = new Error(`Missing username`);
            error.status = 404;
            throw error;
        }
        else {
            const nutritionist = await nutritionistsService.getNutritionistByUsername(username);
            
            if (!nutritionist) {
                const error = new Error(`There is no nutritionist with username ${username}`);
                error.status = 404;
                throw error;
            }
            
            res.status(200).json(nutritionist); 
        } 
    } catch(err) {
           next(err); 
    }
}

const addNewNutritionist = async (req, res, next) => {

    const {username, name, surname, email, password, gender, city, municipality, dateOfBirth, imgUrl, category, approximatePrice} = req.body;
    try {

        if(!username || !email || !password || !name || !surname 
            || !city || !municipality || !dateOfBirth || !gender || !category || !approximatePrice
            || !validator.isEmail(email) || !validator.isAlphanumeric(username)) {

            const error = new Error('Forwarded data about nutritionist is not valid');
            error.status = 400;
            throw error;
        }

        const nutritionist = await nutritionistsService.getNutritionistByUsername(username)
        if (nutritionist) {
            const error = new Error(`Username ${username} is already taken`);
            error.status = 403;
            throw error;
        }


        const token = await nutritionistsService.addNewNutritionist(
            username,
            name,
            surname,
            email,
            password,
            gender,
            city,
            municipality,
            dateOfBirth,
            imgUrl,
            category,
            -1.0,
            approximatePrice
        );
        res.status(201).json({token: token});
        }catch(error){
            next(error);
        }

};

const changeNutririonistPassword = async (req, res, next) => {
    const username = req.params.username;
    const {oldPassword, newPassword} = req.body;

    try {

        if (username != req.username) {
            const error = new Error(`No authorization`);
            error.status = 500;
            throw error;
        }
        
        if (!username || !oldPassword || !newPassword) {
            const error = new Error('Missing required parameter(s)');
            error.status = 400;
            throw error;
        }
        const nutritionist = await nutritionistsService.getNutritionistByUsername(username);
        if (!nutritionist) {
            const error = new Error(`There is no nutritionist with username ${username}`);
            error.status = 404;
            throw error;
        }

        const token = await nutritionistsService.changeNutritionistPassword(username, oldPassword, newPassword);
        if (token === null) {
            const error = new Error('Wrong password');
            error.status = 401;
            throw error;
        }

        return res.status(200).json({
            token: token,
          });
    } catch (error) {
        next(error);
    }

};


const deleteNutritionistByUsername = async (req, res, next) =>{
    
    const username = req.params.username;

    try{
        if (!username) {
            const error = new Error('Missing rusername');
            error.status = 400;
            throw error;
        }
        else {
            const nutritionist = await nutritionistsService.getNutritionistByUsername(username);

            if (!nutritionist) {
                const error = new Error(`There is no nutritionist with username ${username}`);
                error.status = 404;
                throw error;
            }
            nutritionistsService.deleteNutritionistByUsername(username);
            res.status(200).json({message: `Nutritionist ${username} is successfully deleted`});
        }
    } catch(error){
        next(error);
    } 
};


const login = async (req, res, next) => {

    const {username, password} = req.body;
    
    try {
        if (!username || !password) {
            const error = new Error('Missing username and/or password');
            error.status = 400;
            throw error;
        }

        const nutritionist = await nutritionistsService.getNutritionistByUsername(username);
        if (!nutritionist) {
            const error = new Error(`Nutritionist ${username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const isMatched = await bcrypt.compare(password, nutritionist.password);
        if (!isMatched) {
            const error = new Error(`Wrong password`);
            error.status = 401;
            throw error;
        }
        else {
            const token = jwtAuth.generateToken({
                id: nutritionist._id,
                username: username,
                name: nutritionist.name,
                surname: nutritionist.surname, 
                email: nutritionist.email,
                gender: nutritionist.gender,
                city: nutritionist.location.city,
                municipality: nutritionist.location.municipality,
                dateOfBirth: nutritionist.dateOfBirth,
                imgUrl: nutritionist.imgUrl,
                category: nutritionist.category,
                score: nutritionist.score,
                approximatePrice: nutritionist.approximatePrice
            });
            return res.status(201).json({
                token: token      
            });
        }

    } catch (error) {
        next(error);
        console.log(error);
    }

};

const deleteFavRecipe = async(req,res,next) => {
    const {username, idRecipe} = req.params;
    try{
        const {id} = await Recipe.findOne({idRecipe: idRecipe},'_id').exec();
        const updatedNutritionist = await Nutritionist.findOneAndUpdate(
            { username: username},
            { $pull: {recipes: id}},
             { new: true,
               useFindAndModify: false}
        ).exec();
        res.status(201).json(updatedNutritionist);
    }
    catch(error){
        next(error)
    }

};

const getFavRecipes = async(req,res,next) => {
    const username = req.params.username;
    
    try {
        const recipes =  await Nutritionist.findOne({username: username},'recipes').populate('recipes').exec();
        res.status(200).json(recipes["recipes"]);
    } catch (error) {
        next(error)
    }

};

const addNewFavRecipe = async(req,res,next) => {
    const username = req.params.username;    
    const idRecipe = req.body.idRecipe;
   
    try {
        const {id} = await Recipe.findOne({idRecipe: idRecipe},'_id').exec();
         const updatedNutritionist = await Nutritionist.findOneAndUpdate(
            { username: username }, 
            { $push: { recipes : id} },
            { new: true,
              useFindAndModify: false}
            ).exec();
        res.status(201).json(updatedNutritionist);
         
    } catch (error) {
        next(error)
    }
};

  const changeNutritionistProfileImage = async (req, res, next) => {

    const username = req.username;
  
    try {
      await uploadFile(req, res);
  
      if (req.file == undefined) {
        const error = new Error('Upload a file!');
        error.status = 400;
        throw error;
      }
  
      const imgUrl = req.file.filename;
      const token = await nutritionistsService.changeNutritionistProfileImage(username, imgUrl);
  
      return res.status(200).json({
        token: token,
      });
    } catch (err) {
      next(err);
    }
  };


  async function paginateThroughProducts(page = 1, limit = 10, filter, sort) {
    return await Nutritionist.paginate(filter, { page, limit, sort: sort, projection: '-timestamp' });
  }

module.exports = {
    getAllNutritionists,
    addNewNutritionist,
    getNutritionistByUsername,
    changeNutririonistPassword,
    deleteNutritionistByUsername,
    addNewFavRecipe,
    getFavRecipes,
    deleteFavRecipe,
    login,
    changeNutritionistProfileImage
};