const customersService = require('../services/customersService');
const trainersService = require('../services/trainersService');
const nutritionistsService = require('../services/nutritionistsService');

const Customer = require('../models/customerModel');
const Recipe = require('../models/recipeModel');
const Trainer = require('../models/trainerModel');
const Nutritionist = require('../models/nutritionistModel');

const { uploadFile } = require('../controllers/uploadController');
const validator = require('validator');
const bcrypt = require('bcrypt');
const jwtAuth = require('../utils/authentication');


const getCustomerByUsername = async (req, res, next) => {

    const username = req.params.username;
    try {   
        if (!username) {
            const error = new Error('Missing username');
            error.status = 400;
            throw error;
        }
        else {
            const customer = await customersService.getCustomerByUsername(username);

            if (!customer) {
                const error = new Error(`There is no customer with username ${username}`);
                error.status = 404;
                throw error;
            }
            
            res.status(200).json(customer);  
        } 
    } catch(error) {
           next(error); 
    }
}


const addNewCustomer = async (req, res, next) => {

    const {username, name, surname, email, password, gender, city, municipality, dateOfBirth, imgUrl} = req.body;

    try {

        if(!username || !email || !password || !name || !surname 
            || !city || !municipality || !dateOfBirth || !gender
            || !validator.isEmail(email) || !validator.isAlphanumeric(username)) {

            const error = new Error('Forwarded data about customer is not valid');
            error.status = 400;
            throw error;
        }

        const customer = await customersService.getCustomerByUsername(username);
        if (customer) {
            const error = new Error(`Username ${username} is already taken`);
            error.status = 403;
            throw error;
        }

        const token = await customersService.addNewCustomer(
            username,
            name,
            surname,
            email,
            password,
            gender,
            city,
            municipality,
            dateOfBirth,
            imgUrl
        );

        res.status(201).json({token: token});

        } catch(error){
            console.log(error);
            next(error);
            console.log(error);
        }
};

const changeCustomerProfileImage = async (req, res, next) => {

    const username = req.username;
  
    try {
      await uploadFile(req, res);
      console.log(req.file);
  
      if (req.file == undefined) {
        const error = new Error('Please upload a file!');
        error.status = 400;
        throw error;
      }
  
      const imgUrl = req.file.filename;
      const token = await customersService.changeCustomerProfileImage(username, imgUrl);
  
      return res.status(200).json({
        token: token,
      });
    } catch (err) {
      next(err);
    }
  };

const changeCustomerPassword = async (req, res, next) => {
    const username = req.params.username;
    const {oldPassword, newPassword} = req.body;

    try {

        if (username != req.username) {
            const error = new Error(`No authorization`);
            error.status = 500;
            throw error;
        }
        
        if (!username || !oldPassword || !newPassword) {
            const error = new Error('Missing required parameter(s)');
            error.status = 400;
            throw error;
        }
        const customer = await customersService.getCustomerByUsername(username);
        if (!customer) {
            const error = new Error(`Customer ${username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const token = await customersService.changeCustomerPassword(username, oldPassword, newPassword);

        if (token === null) {
            const error = new Error('Wrong password');
            error.status = 401;
            throw error;
        }

        return res.status(200).json({
            token: token,
          });
       
    } catch (error) {
        next(error);
    }

}

const rateTrainer = async (req, res, next) => {
    const trainerUsername = req.params.trainerUsername;
    const {mark} = req.body;

    try {  
        const trainer = await trainersService.getTrainerByUsername(trainerUsername);
        if (!trainer) {
            const error = new Error(`Username ${trainer.username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const updatedCustomer = await trainersService.changeTrainerMarks(trainerUsername,
            mark);
        if (!updatedCustomer) {
            const error = new Error('Rate trainer failed');
            error.status = 403;
            throw error;
        }
        else
            res.status(200).json(updatedCustomer);
    } catch (error) {
        next(error);
    }

}

const rateNutritionist = async (req, res, next) => {
    const nutritionistUsername = req.params.nutritionistUsername;
    const {mark} = req.body;

    try {  
        const nutritionist = await nutritionistsService.getNutritionistByUsername(nutritionistUsername);
        if (!nutritionist) {
            const error = new Error(`Username ${nutritionist.username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const updatedNutritionist = await nutritionistsService.changeNutritionistMarks(
            nutritionistUsername, mark);
        if (!updatedNutritionist) {
            const error = new Error('Rate nutritionist failed');
            error.status = 403;
            throw error;
        }
        else
            res.status(200).json(updatedNutritionist);
    } catch (error) {
        next(error);
    }

}

const deleteCustomerByUsername = async (req, res, next) => {

    const username = req.params.username;

    try{
        if (!username) {
            const error = new Error(`Mising username`);
                error.status = 404;
                throw error;
        }
        else {
            const customer = await customersService.getCustomerByUsername(username);

            if (!customer) {
                const error = new Error(`There is no customer with username ${username}`);
                error.status = 404;
                throw error;
            }
            await customersService.deleteCustomerByUsername(username);
            res.status(200).json({message: `Customer ${username} is successfully deleted`});
        }
    } catch(error){
        next(error);
        console.log(error);
    } 
};

const login = async (req, res, next) => {

    const {username, password} = req.body;
  
    try {
        if (!username || !password) {
            const error = new Error('Missing username and/or password');
            error.status = 400;
            throw error;
        }

        const customer = await customersService.getCustomerByUsername(username);
        if (!customer) {
            const error = new Error(`Customer ${username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const isMatched = await bcrypt.compare(password, customer.password);
        if (!isMatched) {
            const error = new Error(`Wrong password`);
            error.status = 404;
            throw error;
        }
        else {

            const token = jwtAuth.generateToken({
                id: customer._id,
                username: customer.username,
                name: customer.name,
                surname: customer.surname, 
                email: customer.email,
                gender: customer.gender,
                city: customer.location.city,
                municipality: customer.location.municipality,
                dateOfBirth: customer.dateOfBirth,
                imgUrl: customer.imgUrl
            });
            return res.status(201).json({
                token: token      
            });
        }

    } catch (error) {
        next(error);
    }

};

const deleteFavRecipe = async(req,res,next) => {
    const {username, idRecipe} = req.params;
    console.log(username + " " +idRecipe)
    try{
        const {id} = await Recipe.findOne({idRecipe: idRecipe},'_id').exec();
        console.log(id)
        const updatedCustomer = await Customer.findOneAndUpdate(
            { username: username},
            { $pull: {recipes: id}},
             { new: true,
               useFindAndModify: false}
        ).exec();
        console.log(updatedCustomer["recipes"])
        res.status(201).json(updatedCustomer);
    }
    catch(error){
        next(error)
    }

};

const getFavRecipes = async(req,res,next) => {
    const username = req.params.username;
   
    try {
        const recipes =  await Customer.findOne({username: username},'recipes').populate('recipes').exec();
        res.status(200).json(recipes["recipes"]);
    } catch (error) {
        next(error)
    }

};

const addNewFavRecipe = async(req,res,next) => {
    
    const username = req.params.username;  
    const idRecipe = req.body.idRecipe;
   
    try {
        const {id} = await Recipe.findOne({idRecipe: idRecipe},'_id').exec();
         const updatedCustomer = await Customer.findOneAndUpdate(
            { username: username }, 
            { $push: { recipes : id} },
            { new: true,
              useFindAndModify: false}
            ).exec();
      
        res.status(201).json(updatedCustomer);
         
    } catch (error) {
        console.log(error);
        next(error)
    }
};


const addNewTrainerToList = async(req,res,next) => {
    const username = req.params.username;
    const trainerId = req.params.trainerId;

    try {
        
        if (!username || !trainerId) {
            const error = new Error('Missing required parameter(s)');
            error.status = 400;
            throw error;
        }
        const customer = await customersService.getCustomerByUsername(username);
        if (!customer) {
            const error = new Error(`Customer ${username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const updatedCustomer = await customersService.addNewTrainerToList(username, trainerId);
        if (!updatedCustomer) {
            const error = new Error('Adding trainer to the list failed!');
            error.status = 403;
            throw error;
        }
        else
            res.status(200).json(updatedCustomer);
    } catch (error) {
        next(error);
    }
};

const addNewNutritionistToList = async(req,res,next) => {
    const username = req.params.username;
    const nutritionistId = req.params.nutritionistId;

    try {
        
        if (!username || !nutritionistId) {
            console.log(username, nutritionistId)
            const error = new Error('Missing required parameter(s)');
            error.status = 400;
            throw error;
        }
        const customer = await customersService.getCustomerByUsername(username);
        if (!customer) {
            const error = new Error(`Customer ${username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const updatedCustomer = await customersService.addNewNutritionistToList(username, nutritionistId);
        if (!updatedCustomer) {
            const error = new Error('Adding nutritionist to the list failed!');
            error.status = 403;
            throw error;
        }
        else
            res.status(200).json(updatedCustomer);
    } catch (error) {
        next(error);
    }
};

const getCustomersTrainerList = async(req,res,next) => {

    const username = req.params.username;

    try {
        if (!username) {
            const error = new Error('Missing username');
            error.status = 400;
            throw error;
        }
        else {
            const trainers = await customersService.getCustomersTrainerList(username);
            res.status(200).json(trainers);
        }

    } catch (error) {
        next(error);
    }
};

const getCustomersNutritionistList = async(req,res,next) => {

    const username = req.params.username;

    try {
        if (!username) {
            const error = new Error('Missing username');
            error.status = 400;
            throw error;
        }
        else {
            const nutritionists = await customersService.getCustomersNutritionistList(username);
            res.status(200).json(nutritionists);
        }

    } catch (error) {
        next(error);
    }
};

const deleteTrainerFromCustomerTrainerList = async(req,res,next) => {

    const username = req.params.username;
    const trainerId = req.params.trainerId;
   

    try {
        
        if (!username || !trainerId) {
            const error = new Error('Missing required parameter(s)');
            error.status = 400;
            throw error;
        }
        const customer = await customersService.getCustomerByUsername(username);
        if (!customer) {
            const error = new Error(`Customer ${username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const updatedCustomer = await customersService.deleteTrainerFromCustomerTrainerList(username, trainerId);
        if (!updatedCustomer) {
            const error = new Error('Deleting trainer from the list failed!');
            error.status = 403;
            throw error;
        }
        else
            res.status(200).json(updatedCustomer);
    } catch (error) {
        next(error);
    }

};


const deleteNutritionistFromCustomerNutritionistList = async(req,res,next) => {


    const username = req.params.username;
    const nutritionistId = req.params.nutritionistId;
    

    try {
        
        if (!username || !nutritionistId) {
            const error = new Error('Missing required parameter(s)');
            error.status = 400;
            throw error;
        }
        const customer = await customersService.getCustomerByUsername(username);
        if (!customer) {
            const error = new Error(`Customer ${username} doesn't exist`);
            error.status = 404;
            throw error;
        }

        const updatedCustomer = await customersService.deleteNutritionistFromCustomerNutritionistList(username, nutritionistId);
        if (!updatedCustomer) {
            const error = new Error('Deleting nutritionist from the list failed!');
            error.status = 403;
            throw error;
        }
        else
            res.status(200).json(updatedCustomer);
    } catch (error) {
        next(error);
    }

};

module.exports = {
    addNewCustomer,
    getCustomerByUsername,
    deleteCustomerByUsername,
    changeCustomerPassword,
    changeCustomerProfileImage,
    login,
    rateTrainer,
    rateNutritionist,
    addNewFavRecipe,
    deleteFavRecipe,
    getFavRecipes,
    addNewTrainerToList,
    addNewNutritionistToList,
    getCustomersTrainerList,
    getCustomersNutritionistList,
    deleteTrainerFromCustomerTrainerList,
    deleteNutritionistFromCustomerNutritionistList
};

