const Recipe = require('../models/recipeModel');
const mongoose = require('mongoose');
const axios = require("axios").default;


const addNewFavRecipe = async(req,res,next) => {
    const {idRecipe, name, ingredients,instructions,prepTime,score, dishTypes, image} = req.body;
    try{
        console.log(idRecipe);
        const recipe = await Recipe.findOne({idRecipe: idRecipe}).exec();
        if(!recipe){
            const newRecipe = new Recipe({
                _id: new mongoose.Types.ObjectId(),
                idRecipe: idRecipe,
                name: name,
                ingredients: ingredients,
                instructions: instructions,
                prepTime: prepTime,
                score: score,
                dishTypes: dishTypes,
                image: image
            });
        console.log(newRecipe);
        await newRecipe.save();
        next();
        }
        else{
            next();
        }
      
    }
    catch(err){
        next(err);
    }
    
}



const getRecipe = async(req,res,next) => {

    const ingredients = req.params.ingredients;
    console.log(ingredients);
    
    const options = {
        method: 'GET',
        url: 'https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/findByIngredients',
        params: {
        ingredients: ingredients,
        number: '5',
        ignorePantry: 'true',
        ranking: '1'
        },
        headers: {
        'x-rapidapi-key': process.env.API_ID,
        'x-rapidapi-host': 'spoonacular-recipe-food-nutrition-v1.p.rapidapi.com'
        }
    };
    axios.request(options).then(function (response) {
        res.status(200).json(response.data);
        console.log(response.headers);
    }).catch(function (error) {
        next(error);
        console.error(error);
    });
    
}
    
const getRecipeInfo = async(req,res,next) => {
    const id = req.params.id;
    const options = {
        method: 'GET',
        url: `https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/${id}/information`,
        headers: {
        'x-rapidapi-key': process.env.API_ID,
        'x-rapidapi-host': 'spoonacular-recipe-food-nutrition-v1.p.rapidapi.com'
        }
    };
    axios.request(options).then(function (response) {
        res.status(200).json(response.data);
        console.log(response.headers);
    }).catch(function (error) {
        next(error);
        console.error(error);
    });
}
    

module.exports = {
    getRecipe,
    addNewFavRecipe,
    getRecipeInfo

};