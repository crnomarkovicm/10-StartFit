const express = require('express');
const controller = require('../../controllers/contactFormController');

// router for http://localhost:3000/api/contactForm
const router = express.Router();

router.post('/sendmail', controller.sendMail);

module.exports = router;