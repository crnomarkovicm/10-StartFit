const express = require('express');
const controller = require('../../controllers/trainersController');
const {verifyToken} = require('../../utils/authentication');
const recipeController = require('../../controllers/recipesController')

// router for http://localhost:3000/api/trainers
const router = express.Router();

router.post('/', controller.getAllTrainers);

router.get('/username/:username', controller.getTrainerByUsername);

router.get('/username/:username/recipes', controller.getFavRecipes);

router.patch('/username/:username', verifyToken, controller.changeTrainerPassword);
router.patch('/profile-image', verifyToken, controller.changeTrainerProfileImage);

router.put('/username/:username/changeScore', verifyToken, controller.changeTrainerScore);

router.put('/username/:username/recipes', [recipeController.addNewFavRecipe, controller.addNewFavRecipe]);

router.put('/username/:username/recipes/:idRecipe', controller.deleteFavRecipe);

router.post('/register', controller.addNewTrainer);

router.post('/login', controller.login);

router.delete('/username/:username', verifyToken, controller.deleteTrainerByUsername);


module.exports = router;
