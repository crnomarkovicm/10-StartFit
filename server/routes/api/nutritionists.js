const express = require('express');
const controller = require('../../controllers/nutritionistsController');
const {verifyToken} = require('../../utils/authentication');
const recipeController = require('../../controllers/recipesController')


// router for http://localhost:3000/api/nutritionists
const router = express.Router();

router.post('/', controller.getAllNutritionists);

router.get('/username/:username', controller.getNutritionistByUsername);

router.get('/username/:username/recipes', controller.getFavRecipes);

router.patch('/username/:username', verifyToken, controller.changeNutririonistPassword);

router.patch('/profile-image', verifyToken, controller.changeNutritionistProfileImage);

router.put('/username/:username/recipes', [recipeController.addNewFavRecipe, controller.addNewFavRecipe]);

router.put('/username/:username/recipes/:idRecipe', controller.deleteFavRecipe);

router.post('/register', controller.addNewNutritionist);

router.post('/login', controller.login);

router.delete('/username/:username', verifyToken, controller.deleteNutritionistByUsername);


module.exports = router;