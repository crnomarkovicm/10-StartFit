const express = require('express');
const controller = require('../../controllers/customersController');
const {verifyToken} = require('../../utils/authentication');
const recipeController = require('../../controllers/recipesController')

// router for http://localhost:3000/api/customers
const router = express.Router();

router.post('/register', controller.addNewCustomer);

router.patch('/username/:username', verifyToken, controller.changeCustomerPassword);

router.patch('/profile-image', verifyToken, controller.changeCustomerProfileImage);

router.delete('/username/:username', verifyToken, controller.deleteCustomerByUsername);

router.post('/login', controller.login);

router.put('/username/:username/addTrainer/:trainerId', controller.addNewTrainerToList);
router.put('/username/:username/addNutritionist/:nutritionistId', controller.addNewNutritionistToList);

router.get('/username/:username/trainers', controller.getCustomersTrainerList);
router.get('/username/:username/nutritionists', controller.getCustomersNutritionistList);

router.put('/rateTrainer/:trainerUsername', controller.rateTrainer);
router.put('/rateNutritionist/:nutritionistUsername', controller.rateNutritionist);

router.put('/username/:username/deleteTrainer/:trainerId', controller.deleteTrainerFromCustomerTrainerList);
router.put('/username/:username/deleteNutritionist/:nutritionistId', controller.deleteNutritionistFromCustomerNutritionistList);

router.put('/username/:username/recipes', [recipeController.addNewFavRecipe, controller.addNewFavRecipe]);
router.get('/username/:username/recipes', controller.getFavRecipes);
router.put('/username/:username/recipes/:idRecipe', controller.deleteFavRecipe);

module.exports = router;