const express = require('express');
const controller = require('../../controllers/recipesController');

// router for http://localhost:3000/api/recipes
const router = express.Router();

router.get('/ingredients/:ingredients', controller.getRecipe);
router.get('/:id',controller.getRecipeInfo);


module.exports = router;