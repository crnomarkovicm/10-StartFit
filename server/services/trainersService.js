const Trainer = require('../models/trainerModel');
const jwtAuth = require('../utils/authentication');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const hashPassword = async (password) => {
    const SALT_ROUNDS = 10;
    return await bcrypt.hash(password, SALT_ROUNDS);
};


const getAllTrainers = async () => {
    const trainers = await Trainer.find({}).exec();
    return trainers;
};

const getTrainerByUsername = async (username) => {
    const trainers = await Trainer.findOne({username: username}).exec();
    return trainers;
};

const addNewTrainer = async (username, name, surname, 
    email, password, gender,
     city, municipality, dateOfBirth, imgUrl, category, score, price) => {

        const hashedPassword = await hashPassword(password);

        const newTrainer = new Trainer({
            _id: new mongoose.Types.ObjectId(), 
            username,
            name,
            surname,
            password: hashedPassword,
            email,
            dateOfBirth,
            gender,
            location: {
                city: city,
                municipality: municipality
            },
            category,
            score,
            imgUrl,
            approximatePrice: price
        });

        await newTrainer.save();
        return jwtAuth.generateToken({
            id: newTrainer._id,
            username: username,
            name: name,
            surname: surname, 
            email: email,
            gender: gender,
            city: city,
            municipality: municipality,
            dateOfBirth: dateOfBirth,
            imgUrl: imgUrl,
            category: category,
            score: score,
            approximatePrice: price
        });

};

const changeTrainerPassword = async (username, oldPassword, newPassword) => {

    const trainers = await Trainer.findOne({username:username}).exec();
    const passwordMatch = await bcrypt.compare(oldPassword, trainers.password);

    if (!passwordMatch){
        return null;
    }
    const hashedPassword = await hashPassword(newPassword);
    const updatedTrainer = await Trainer.findOneAndUpdate(
        { username: username },
        { $set: { password: hashedPassword } },
        { new: true });
        return jwtAuth.generateToken({
            id: updatedTrainer._id,
            username: updatedTrainer.username,
            name: updatedTrainer.name,
            surname: updatedTrainer.surname, 
            email: updatedTrainer.email,
            gender: updatedTrainer.gender,
            city: updatedTrainer.location.city,
            municipality: updatedTrainer.location.municipality,
            dateOfBirth: updatedTrainer.dateOfBirth,
            imgUrl: updatedTrainer.imgUrl,
            category: updatedTrainer.category,
            score: updatedTrainer.score,
            approximatePrice: updatedTrainer.approximatePrice
        });
};

const changeTrainerMarks = async (username, mark) => {

    const trainer = await Trainer.findOne({username:username}).exec();
    
    const pom = parseInt(mark);
    const updatedTrainer = await Trainer.findOneAndUpdate(
        { username: username },
        { $set: { "numberOfCustomer": trainer.numberOfCustomer + 1,
                  "marks": trainer.marks + pom} },
        { new: true });   
    return updatedTrainer;
};

const changeTrainerScore = async (username) => {

    const trainer = await Trainer.findOne({username:username}).exec();
    
    const updatedTrainer = await Trainer.findOneAndUpdate(
        { username: username },
        { $set: { "score": trainer.marks/trainer.numberOfCustomer*1.0} },
        { new: true });   
  
    return updatedTrainer;
};


const deleteTrainerByUsername = async (username) => {
    await Trainer.findOneAndDelete({username: username}).exec();
};

async function changeTrainerProfileImage(username, imgUrl) {
    const tr = await getTrainerByUsername(username);
    tr.imgUrl = imgUrl;
    await tr.save();

    return jwtAuth.generateToken({
        id: tr._id,
        username: tr.username,
        name: tr.name,
        surname: tr.surname, 
        email: tr.email,
        gender: tr.gender,
        city: tr.location.city,
        municipality: tr.location.municipality,
        dateOfBirth: tr.dateOfBirth,
        imgUrl: tr.imgUrl,
        category: tr.category,
        score: tr.score,
        approximatePrice: tr.approximatePrice
    });
};

module.exports = {
    getAllTrainers,
    addNewTrainer,
    getTrainerByUsername,
    deleteTrainerByUsername,
    changeTrainerPassword,
    changeTrainerMarks,
    changeTrainerScore,
    changeTrainerProfileImage
};