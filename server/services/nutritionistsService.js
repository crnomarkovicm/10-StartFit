const Nutritionist = require('../models/nutritionistModel');
const jwtAuth = require('../utils/authentication');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const hashPassword = async (password) => {
    const SALT_ROUNDS = 10;
    return await bcrypt.hash(password, SALT_ROUNDS);
};


const getAllNutritionists = async () => {
    const nutritionists = await Nutritionist.find({}).exec();
    return nutritionists;
};

const getNutritionistByUsername = async (username) => {
    const nutritionists = await Nutritionist.findOne({username: username}).exec();
    return nutritionists;
};


const addNewNutritionist = async (username, name, surname, 
    email, password, gender,
     city, municipality, dateOfBirth, imgUrl, category, score, price) => {

        const hashedPassword = await hashPassword(password);

        const newNutritionist = new Nutritionist({
            _id: new mongoose.Types.ObjectId(), 
            username,
            name,
            surname,
            password: hashedPassword,
            email,
            dateOfBirth,
            gender,
            location: {
                city: city,
                municipality: municipality
            },
            category,
            score,
            imgUrl,
            approximatePrice: price
        });

        await newNutritionist.save();
        return jwtAuth.generateToken({
            id: newNutritionist._id,
            username: username,
            name: name,
            surname: surname, 
            email: email,
            gender: gender,
            city: city,
            municipality: municipality,
            dateOfBirth: dateOfBirth,
            imgUrl: imgUrl,
            category: category,
            score: score,
            approximatePrice: price
        });

};

const changeNutritionistPassword = async (username, oldPassword, newPassword) => {

    const nutritionists = await Nutritionist.findOne({username:username}).exec();
    const passwordMatch = await bcrypt.compare(oldPassword, nutritionists.password);
    
    if (!passwordMatch){
        return null;
    }
    const hashedPassword = await hashPassword(newPassword);
    const updatedNutritionist = await Nutritionist.findOneAndUpdate(
        { username: username },
        { $set: { password: hashedPassword } },
        { new: true });
        return jwtAuth.generateToken({
            id: updatedNutritionist._id,
            username: updatedNutritionist.username,
            name: updatedNutritionist.name,
            surname: updatedNutritionist.surname, 
            email: updatedNutritionist.email,
            gender: updatedNutritionist.gender,
            city: updatedNutritionist.location.city,
            municipality: updatedNutritionist.location.municipality,
            dateOfBirth: updatedNutritionist.dateOfBirth,
            imgUrl: updatedNutritionist.imgUrl,
            category: updatedNutritionist.category,
            score: updatedNutritionist.score,
            approximatePrice: updatedNutritionist.approximatePrice
        });
};


const changeNutritionistMarks = async (username, mark) => {

    const nutritionist = await Nutritionist.findOne({username:username}).exec();
    const pom = parseInt(mark);
    const updatedNutritionist = await Nutritionist.findOneAndUpdate(
        { username: username },
        { $set: { "numberOfCustomer": nutritionist.numberOfCustomer + 1,
                  "marks": nutritionist.marks + pom} },
        { new: true });   
  
    return updatedNutritionist;
};

const changeNutritionistScore = async (username) => {

    const nutritionist = await Nutritionist.findOne({username:username}).exec();
    
    const updatedNutritionist = await Nutritionist.findOneAndUpdate(
        { username: username },
        { $set: { "score": nutritionist.marks/nutritionist.numberOfCustomer*1.0} },
        { new: true });   
  
    return updatedNutritionist;
};

const deleteNutritionistByUsername = async (username) => {
    await Nutritionist.findOneAndDelete({username: username}).exec();
};

async function changeNutritionistProfileImage(username, imgUrl) {
    const nutr = await getNutritionistByUsername(username);
    nutr.imgUrl = imgUrl;
    await nutr.save();

    return jwtAuth.generateToken({
        id: nutr._id,
        username: nutr.username,
        name: nutr.name,
        surname: nutr.surname, 
        email: nutr.email,
        gender: nutr.gender,
        city: nutr.location.city,
        municipality: nutr.location.municipality,
        dateOfBirth: nutr.dateOfBirth,
        imgUrl: nutr.imgUrl,
        category: nutr.category,
        score: nutr.score,
        approximatePrice: nutr.approximatePrice
    });
};

module.exports = {
    getAllNutritionists,
    addNewNutritionist,
    getNutritionistByUsername,
    deleteNutritionistByUsername,
    changeNutritionistPassword,
    changeNutritionistMarks,
    changeNutritionistScore,
    changeNutritionistProfileImage
};