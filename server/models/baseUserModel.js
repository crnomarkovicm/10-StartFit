const mongoose = require('mongoose');

module.exports = function(paths) {

    const baseUsersSchema = new mongoose.Schema({
        _id: mongoose.Schema.Types.ObjectId,
    
    username: {
        type: String,
        required: true,
    },
    name : {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    dateOfBirth: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    
    location: { 
        city: {
            type: String,
            required: true
        },
        municipality: {
            type: String,
            required: true 
        }
    },
    imgUrl: {
        type: mongoose.Schema.Types.String,
        required: true
      },
    });

    baseUsersSchema.add(paths);
    return baseUsersSchema;

};