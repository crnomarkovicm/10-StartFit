const mongoose = require('mongoose');
const baseUsersSchema = require('./baseUserModel');
const mongoosePaginate = require('mongoose-paginate-v2');

const trainersSchema = new baseUsersSchema({

    category: {
        type: [String],
        required: true
    },

    score: {
        type: mongoose.Schema.Types.Decimal128,
        min: 0.0,
        max: 5.0
    },
    approximatePrice: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    numberOfCustomer: {
        type: Number,
        default: 0
    },
    marks: {
        type: Number,
        default: 0
    },
    recipes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Recipe"
    }],
    
});

trainersSchema.plugin(mongoosePaginate);


const trainersModel = mongoose.model('Trainer', trainersSchema);

  
module.exports = trainersModel;