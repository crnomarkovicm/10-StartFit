const mongoose = require('mongoose');

const recipesSchema = new mongoose.Schema({

    // baza nam dodeljuje id al svaki nas recept koji dohvatamo sa apija ima svoj ID
    _id: mongoose.Schema.Types.ObjectId,
    idRecipe: {
        type: String,
        required: true
    },
    name : {
        type: String,
        required: true
    },
    ingredients: {
        type: [String],
        required: true
    },
    instructions: {
        type: String,
        required: true
    },
    prepTime: { //u minutima
        type: Number,
        required: true
    },
    score: {
        type: Number,
        required: true,
    },
    dishTypes: {
        type: [String],
        required: true
    },
    //gridfs
    image: {
        type: String
    },
    //veza izmedju customera i recepata je vise ka vise, mozda nam ne treba customer
   /* customer: [{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "customers"
    }]*/
    
}, { collection: "recipes" });

const recipesModel = mongoose.model('Recipe',recipesSchema);

module.exports = recipesModel;
