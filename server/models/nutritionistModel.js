const mongoose = require('mongoose');
const baseUsersSchema = require('./baseUserModel');
const mongoosePaginate = require('mongoose-paginate-v2');

const nutritionistsSchema = new baseUsersSchema({

    category: {
        type: [String],
        required: true
    },

    score: {
        type: mongoose.Schema.Types.Decimal128,
        min: 0.0,
        max: 5.0
    },
    approximatePrice: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    numberOfCustomer: {
        type: Number,
        default: 0
    },
    marks: {
        type: Number,
        default: 0
    },
    recipes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Recipe"
    }]

});
nutritionistsSchema.plugin(mongoosePaginate);
const nutritionistsModel = mongoose.model('Nutritionist', nutritionistsSchema);

module.exports = nutritionistsModel;