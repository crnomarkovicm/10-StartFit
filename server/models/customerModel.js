const mongoose = require('mongoose');
const baseUsersSchema = require('./baseUserModel');

const customersSchema = new baseUsersSchema({
    recipes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Recipe"
    }],
    trainers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Trainer"
    }],
    nutritionists: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Nutritionist"
    }]
});

const customersModel = mongoose.model('Customer', customersSchema);

module.exports = customersModel;