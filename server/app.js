// Express.js module
const express = require('express');
const {urlencoded, json} = require('body-parser');
const mongoose = require('mongoose');

const path = require('path');
const fs = require('fs');
global.__uploadDir = path.join(__dirname, 'resources', 'uploads');
if (!fs.existsSync(__uploadDir)) {
  fs.mkdirSync(__uploadDir);
}

// Create app by calling express() function
// App is a web server
const app = express();

// Connect to the database
const databaseString = "mongodb://localhost:27017/startFitDB";
mongoose.connect(databaseString, {
   useNewUrlParser: true,
   useUnifiedTopology: true  
});

mongoose.connection.once('open', function(){
    console.log('Connection successful!');
});

mongoose.connection.on('error', error => {
    console.log('Error: ', error);
});


app.use(json());
app.use(urlencoded({extended: false}));


app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  
    if (req.method === 'OPTIONS') {
      res.header('Access-Control-Allow-Methods', 'OPTIONS, PATCH, GET, POST, PUT, DELETE');
  
      return res.status(200).json({});
    }
  
    next();
  });

const customersAPI = require('./routes/api/customers');
app.use('/api/customers', customersAPI);

const trainersAPI = require('./routes/api/trainers');
app.use('/api/trainers', trainersAPI);

const nutritionistsAPI = require('./routes/api/nutritionists');
app.use('/api/nutritionists', nutritionistsAPI);

const recipesAPI = require('./routes/api/recipes');
app.use('/api/recipes', recipesAPI);

const contactFormAPI = require('./routes/api/contactForm');
app.use('/api/contactForm',contactFormAPI);

app.use(express.static(__uploadDir));

app.use(function (req, res, next){
    const error = new Error("Uknown request!");
    error.status = 405;
    next(error);
});


app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
    res.status(statusCode).json({
            message: error.message,
            status: statusCode,
            stack: error.stack
        
    });
});

module.exports = app;