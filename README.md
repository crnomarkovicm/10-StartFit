# Project StartFit

Web aplikacija koja pomaže ljudima, a najviše studentima, da vode zdrav život. Korisnici sajta imaju mogućnost da ukucaju namirnice koje imaju kod kuće i da kao odgovor dobiju neki zdrav recept. Zatim moguće je pretražiti dostupne trenere i nutricioniste po lokaciji, oceni itd..Sa druge strane i treneri i nutricionisti će imati svoj poseban nalog i na taj način nuditi usluge ostalim korisnicima sajta.

## Tehnologije 📚 :


- Angular (client side)

- HTML, CSS (client side)
 
- Node.js/Express.js (server side)
  
- MongoDB


## Demo snimak

- https://www.youtube.com/watch?v=xcoFknlfrQA


## Pokretanje aplikacije 🔧 :

- git clone https://gitlab.com/matfpveb/projekti/2020-2021/10-StartFit.git

Inicijalizacija baze: 🔨 :

- cd 10-StartFit/backend/server-data
- chmod +x importData.sh
- ./importData.sh

Server side

- cd 10-StartFit/backend
- npm install
- nodemon

Client side

- cd 10-StartFit/client
- npm install
- ng serve

## .env
U .env fajlu potrebno je dodati:

- JWT_SECRET = yourTokenSecret
- API_ID = yourIdFromSpoonacularAPI
- EMAIL = yourEmail
- PASS = yourPasswordForEmail


## API
- Api koji smo koristili: https://spoonacular.com/food-api.
- Uz pomoc njega smo dohvatali razlicite recepte.
- Neophodno je napraviti nalog na sledecem [linku](https://rapidapi.com/spoonacular/api/recipe-food-nutrition/details)
- Nakon toga dobicete id pomocu koga je moguce slati pozive na API.

## Slanje mejlova:
- Ako ne uspe slanje mejla u kontakt formi potrebno je ukljuciti opciju access to less secure apps u nalogu email-a koji ste sacuvali u .env fajlu. 
- Potrebno je otici u account settings >> security i tu cete videti gore pomenutu opciju.

## Opis perzistentnih podataka:
- [BaseUser](PersistentDataDescription/base_user)
- [Customer](PersistentDataDescription/customer)
- [Trainer](PersistentDataDescription/trainerSchema)
- [Nutritionist](PersistentDataDescription/nutrtionistSchema)
- [Recipe](PersistentDataDescription/recipesSchema)

## Developers

- [Milica Sudar, 79/2017](https://gitlab.com/Milica-Sudar)
- [Katarina Simić, 391/2017](https://gitlab.com/SimicKatarina)
- [Emilija Stošić, 154/2017](https://gitlab.com/Emilija100sic)
- [Maja Crnomarković, 21/2017](https://gitlab.com/crnomarkovicm)
- [Marko Babić, 77/2017](https://gitlab.com/markobabic8)
